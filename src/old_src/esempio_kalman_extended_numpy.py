import numpy as np

class ExtendedKalmanFilter:
    def __init__(self, f, h, F_jacobian, H_jacobian, Q, R, x0, P0):
        """
        f: funzione di transizione di stato non lineare
        h: funzione di osservazione non lineare
        F_jacobian: funzione per calcolare la Jacobiana di f
        H_jacobian: funzione per calcolare la Jacobiana di h
        Q: matrice di covarianza del rumore di processo
        R: matrice di covarianza del rumore di osservazione
        x0: stato iniziale
        P0: matrice di covarianza iniziale
        """
        self.f = f
        self.h = h
        self.F_jacobian = F_jacobian
        self.H_jacobian = H_jacobian
        self.Q = Q
        self.R = R
        self.x = x0
        self.P = P0

    def predict(self):
        # Calcolo dello stato previsto attraverso la funzione di transizione non lineare
        self.x = self.f(self.x)
        # Linearizzazione: Jacobiana della funzione di transizione
        F = self.F_jacobian(self.x)
        # Aggiornamento della covarianza di predizione
        self.P = F @ self.P @ F.T + self.Q
        return self.x

    def update(self, z):
        # Calcolo della Jacobiana della funzione di osservazione
        H = self.H_jacobian(self.x)
        # Innovazione o residuo: differenza tra misura e osservazione prevista
        y = z - self.h(self.x)
        # Matrice di covarianza dell'innovazione
        S = H @ self.P @ H.T + self.R
        # Guadagno di Kalman
        K = self.P @ H.T @ np.linalg.inv(S)
        # Aggiornamento dello stato
        self.x = self.x + K @ y
        # Aggiornamento della covarianza
        I = np.eye(self.P.shape[0])
        self.P = (I - K @ H) @ self.P
        return self.x

# Esempio di sistema non lineare: moto circolare semplice
def f(x):
    """Funzione di transizione non lineare: evoluzione dello stato."""
    dt = 1.0  # Passo temporale
    x_new = np.zeros_like(x)
    # Moto circolare con velocita angolare e posizione
    x_new[0] = x[0] + x[2] * np.cos(x[1]) * dt  # x posizione
    x_new[1] = x[1] + x[3] * dt                # angolo
    x_new[2] = x[2]                            # velocita lineare costante
    x_new[3] = x[3]                            # velocita angolare costante
    return x_new

def h(x):
    """Funzione di osservazione non lineare: osserviamo solo la posizione."""
    return np.array([x[0], x[1]])

def F_jacobian(x):
    """Jacobiana della funzione di transizione f."""
    dt = 1.0
    F = np.eye(4)
    F[0, 1] = -x[2] * np.sin(x[1]) * dt  # Derivata di x rispetto ad angolo
    F[0, 2] = np.cos(x[1]) * dt          # Derivata di x rispetto a velocit\`{a}
    F[1, 3] = dt                         # Derivata di angolo rispetto a velocita angolare
    return F

def H_jacobian(x):
    """Jacobiana della funzione di osservazione h."""
    H = np.zeros((2, 4))
    H[0, 0] = 1  # Osserviamo la posizione x
    H[1, 1] = 1  # Osserviamo l'angolo
    return H

# Parametri iniziali
Q = np.eye(4) * 0.1  # Covarianza del rumore di processo
R = np.eye(2) * 0.5  # Covarianza del rumore di osservazione
x0 = np.array([0.0, 0.0, 1.0, 0.1])  # Stato iniziale: [posizione_x, angolo, velocit\`{a}_lineare, velocit\`{a}_angolare]
P0 = np.eye(4) * 1.0  # Covarianza iniziale

# Creazione del filtro di Kalman esteso
ekf = ExtendedKalmanFilter(f, h, F_jacobian, H_jacobian, Q, R, x0, P0)

# Simulazione di osservazioni
observations = [
    np.array([1.0, 0.1]),  # Prima osservazione
    np.array([2.0, 0.2]),  # Seconda osservazione
    np.array([3.0, 0.3])   # Terza osservazione
]

# Applicazione dell'EKF alle osservazioni
for i, z in enumerate(observations):
    print(f"\nPasso {i+1}")
    pred = ekf.predict()
    print(f"Predizione dello stato: {pred}")
    est = ekf.update(z)
    print(f"Stima aggiornata dello stato: {est}")
