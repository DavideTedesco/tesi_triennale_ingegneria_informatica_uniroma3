import numpy as np
from scipy.io.wavfile import read, write
import matplotlib.pyplot as plt
import os
import tempfile

# Funzione del modello di processo
def process_model(x, A_q, b_q, e, w):
    print("[DEBUG] Dentro process_model")
    p = len(w)
    x_pred = np.dot(A_q, x) + np.concatenate((b_q * e, w))
    return x_pred

# Funzione del modello di misura
def measurement_model(x, c, zeta):
    print("[DEBUG] Dentro measurement_model")
    return np.dot(c, x) + zeta

# Linearizzazione del modello di processo
def linearize_process_model(x_hat, A_q, p):
    print("[DEBUG] Dentro linearize_process_model")
    q = len(x_hat)
    F = np.zeros((q, q))
    F[:p, :p] = A_q
    F[p:, :p] = np.eye(p)
    F[p:, p:] = np.eye(p)
    return F

# Calcolare la matrice di covarianza Omega
def calculate_Omega(b_q, sigma_e, sigma_w):
    print("[DEBUG] Dentro calculate_Omega")
    p = len(b_q)-1
    xi = sigma_w ** 2 / sigma_e ** 2
    Omega = np.zeros((p + 1, p + 1))
    Omega[0, 0] = np.dot(b_q, b_q.T)
    Omega[1:, 1:] = xi * np.eye(p)
    print("[DEBUG] Grandezza di Omega ")
    print(len(Omega))
    return Omega

# Passo di previsione del filtro di Kalman
def predict(x_hat_file, Sigma_file, F, Omega, temp_files):
    print("[DEBUG] Dentro la funzione predict")
    x_hat_prev = np.load(x_hat_file)
    Sigma_prev = np.load(Sigma_file)

    x_pred = np.dot(F, x_hat_prev)
    Sigma_pred = np.dot(np.dot(F, Sigma_prev), F.T) + Omega

    x_pred_file = tempfile.NamedTemporaryFile(delete=False, suffix='.npy').name
    Sigma_pred_file = tempfile.NamedTemporaryFile(delete=False, suffix='.npy').name

    np.save(x_pred_file, x_pred)
    np.save(Sigma_pred_file, Sigma_pred)

    temp_files.extend([x_pred_file, Sigma_pred_file])
    return x_pred_file, Sigma_pred_file

# Passo di aggiornamento del filtro di Kalman
def update(x_pred_file, Sigma_pred_file, y, c, sigma_z, d_hat, temp_files):
    print("[DEBUG] Dentro la funzione update")
    x_pred = np.load(x_pred_file)
    Sigma_pred = np.load(Sigma_pred_file)

    epsilon = y - np.dot(c, x_pred)
    print(f"[DEBUG] Epsilon calcolato: {epsilon}")
    if d_hat == 0:
        K = np.dot(Sigma_pred, c) / (np.dot(np.dot(c, Sigma_pred), c.T) + sigma_z)
    else:
        K = np.zeros_like(Sigma_pred)
    x_update = x_pred + np.dot(K, epsilon)
    Sigma_update = np.dot(np.eye(len(x_pred)) - np.dot(K, c), Sigma_pred)

    x_update_file = tempfile.NamedTemporaryFile(delete=False, suffix='.npy').name
    Sigma_update_file = tempfile.NamedTemporaryFile(delete=False, suffix='.npy').name

    np.save(x_update_file, x_update)
    np.save(Sigma_update_file, Sigma_update)

    temp_files.extend([x_update_file, Sigma_update_file])
    return x_update_file, Sigma_update_file

# Rilevamento del click
def detect_click(epsilon, mu, sigma_epsilon_hat):
    print(f"[DEBUG] Dentro detect_click con epsilon: {epsilon}, mu: {mu}, sigma_epsilon_hat: {sigma_epsilon_hat}")
    return 0 if abs(epsilon) <= mu * sigma_epsilon_hat else 1

# Carica un file audio (formato WAV)
print("[DEBUG] Caricando il file audio")
audio_path = 'vynil_mono_cut.wav'  # Sostituisci con il percorso del tuo file WAV
sr, y = read(audio_path)  # sr: frequenza di campionamento, y: dati audio

# Converte stereo in mono se necessario
if len(y.shape) == 2:
    print("[DEBUG] Convertendo stereo in mono")
    y = np.mean(y, axis=1)

# Normalizza il segnale audio se è in formato intero
if y.dtype != np.float32:
    print("[DEBUG] Normalizzando il segnale audio")
    y = y / np.max(np.abs(y))

# Parametri del rumore
sigma_e = 0.5
sigma_w = 0.2
sigma_z = 0.1

# Genera il segnale rumoroso
print("[DEBUG] Generando il segnale rumoroso")
T = len(y)
e = np.random.normal(0, sigma_e, T)
w = np.random.normal(0, sigma_w, T)
z = np.random.normal(0, sigma_z, T)

v = np.zeros(T)
click_probability = 0.05
for t in range(T):
    if np.random.rand() < click_probability:
        v[t] = np.random.choice([-1, 1]) * np.random.uniform(10, 50)

y_noisy = y + z + v

# Inizializza le variabili del filtro di Kalman
q = len(y)
x_hat = np.zeros(q)
Sigma = np.eye(q)

# Salva le variabili iniziali in file temporanei
temp_files = []
x_hat_file = tempfile.NamedTemporaryFile(delete=False, suffix='.npy').name
Sigma_file = tempfile.NamedTemporaryFile(delete=False, suffix='.npy').name

print("[DEBUG] Salvando le matrici iniziali")
np.save(x_hat_file, x_hat)
np.save(Sigma_file, Sigma)

temp_files.extend([x_hat_file, Sigma_file])

restored_audio = []
epsilon_list = []

# Itera sul segnale per il restauro
print("[DEBUG] Inizio del ciclo di restauro del segnale")
for t in range(T):
    print(f"[DEBUG] Iterazione {t+1}/{T}")
    x_pred_file, Sigma_pred_file = predict(x_hat_file, Sigma_file, np.eye(q), calculate_Omega(np.ones(q), sigma_e, sigma_w), temp_files)
    print("[DEBUG]dim x_pred_file")
    print(len(x_pred_file))
    y_pred = measurement_model(np.load(x_pred_file), np.array([1]), 0)
    epsilon = y_noisy[t] - y_pred
    epsilon_list.append(epsilon)

    d_hat = detect_click(epsilon, mu=3, sigma_epsilon_hat=np.sqrt(np.load(Sigma_pred_file)[0, 0]))
    x_update_file, Sigma_update_file = update(x_pred_file, Sigma_pred_file, y_noisy[t], np.array([1]), sigma_z, d_hat, temp_files)

    restored_audio.append(y_pred)
    x_hat_file = x_update_file
    Sigma_file = Sigma_update_file

print("[DEBUG] Restauro del segnale completato")

# Salva l'audio restaurato
restored_audio = np.array(restored_audio)
restored_audio = restored_audio / np.max(np.abs(restored_audio))
write('restored_audio.wav', sr, (restored_audio * 32767).astype(np.int16))

# Traccia i risultati
print("[DEBUG] Tracciando i risultati")
time = np.linspace(0, len(y) / sr, num=len(y))
plt.figure(figsize=(10, 8))
plt.subplot(3, 1, 1)
plt.plot(time, y, color='blue', alpha=0.7, label='Audio originale')
plt.xlabel('Tempo (s)')
plt.ylabel('Ampiezza')
plt.legend()

plt.subplot(3, 1, 2)
plt.plot(time, y_noisy, color='red', alpha=0.7, label='Audio rumoroso')
plt.xlabel('Tempo (s)')
plt.ylabel('Ampiezza')
plt.legend()

plt.subplot(3, 1, 3)
plt.plot(time, restored_audio, color='green', linestyle='--', label='Audio restaurato')
plt.xlabel('Tempo (s)')
plt.ylabel('Ampiezza')
plt.legend()

plt.tight_layout()
plt.show()

# Pulizia dei file temporanei
print("[DEBUG] Pulizia dei file temporanei")
for temp_file in temp_files:
    if os.path.exists(temp_file):
        os.remove(temp_file)

print("[DEBUG] Programma completato")
