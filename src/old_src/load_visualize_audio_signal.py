import librosa
# https://librosa.org/doc/latest/genindex.html
import numpy as np
import matplotlib.pyplot as plt

# Load the audio file
audio_path = 'drumsMlp.mp3'
y, sr = librosa.load(audio_path)

# Parameters for noise generation
sigma_e = 0.5  # Variance of white noise sequence e(t)
sigma_w = 0.2  # Variance of white noise sequence w(t)
sigma_z = 0.1  # Variance of broadband noise z(t)

# Generate white noise sequences e(t), w(t), and z(t)
T = len(y)
e = np.random.normal(0, sigma_e, T)
w = np.random.normal(0, sigma_w, T)
z = np.random.normal(0, sigma_z, T)

# Generate impulsive noise v(t)
v = np.zeros(T)
# Assuming clicks occur randomly with a certain probability
click_probability = 0.05
for t in range(T):
    if np.random.rand() < click_probability:
        v[t] = np.random.choice([-np.inf, np.inf])  # Infinite variance for clicks

# Add noise components to the audio signal to obtain the noisy signal
y_noisy = y + z + v

# Calculate the time array for plotting
time = librosa.times_like(y, sr=sr)

# Plot original and noisy waveforms in subplots
plt.figure(figsize=(14, 7))

# Original waveform subplot
plt.subplot(2, 1, 1)
plt.plot(time, y, color='blue')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.title('Original Audio Waveform')

# Noisy waveform subplot
plt.subplot(2, 1, 2)
plt.plot(time, y_noisy, color='maroon')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.title('Noisy Audio Waveform')

plt.tight_layout()  # Adjust subplot layout to prevent overlap
plt.show()
