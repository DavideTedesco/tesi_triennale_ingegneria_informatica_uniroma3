import numpy as np
import librosa
import matplotlib.pyplot as plt
import os
import soundfile as sf

# Load the audio file
audio_path = 'drumsMlp.mp3'
y, sr = librosa.load(audio_path)

# Parameters for noise generation
sigma_e = 0.5  # Variance of white noise sequence e(t)
sigma_w = 0.2  # Variance of white noise sequence w(t)
sigma_z = 0.1  # Variance of broadband noise z(t)

# Generate white noise sequences e(t), w(t), and z(t)
T = len(y)
e = np.random.normal(0, sigma_e, T)
w = np.random.normal(0, sigma_w, T)
z = np.random.normal(0, sigma_z, T)

# Generate impulsive noise v(t)
v = np.zeros(T)
# Assuming clicks occur randomly with a certain probability
click_probability = 0.05
for t in range(T):
    if np.random.rand() < click_probability:
        v[t] = np.random.choice([-np.inf, np.inf])  # Infinite variance for clicks

# Add noise components to the audio signal to obtain the noisy signal
y_noisy = y + z + v

# Calculate the time array for plotting
time = librosa.times_like(y, sr=sr)

# Reconstruction using Extended Kalman Filter
# Initialize state vector and covariance matrix
q = len(y)
x_hat = np.zeros(q)
Sigma = np.eye(q)

# Initialize lists to store restored audio
restored_audio = []

# Perform prediction and update steps for each time step
for t in range(T):
    # Predict step
    x_pred, Sigma_pred = predict(x_hat, Sigma, F, Omega)

    # Measurement model
    y_pred = measurement_model(x_pred, np.array([1]), 0)

    # Update step
    d_hat = detect_click(epsilon_list[t], mu, np.sqrt(Sigma_pred[0, 0]))
    if d_hat == 0:
        x_update, Sigma_update = update(x_pred, Sigma_pred, y_noisy[t], np.array([1]), sigma_z, d_hat)
    else:
        x_update, Sigma_update = ewls_update(x_pred, Sigma_pred, epsilon_list[t], np.array([1]), gamma)

    # Store restored audio
    restored_audio.append(y_pred)

    # Update state vector and covariance matrix
    x_hat = x_update
    Sigma = Sigma_update

# Plotting
plt.figure(figsize=(10, 8))

# Original audio subplot
plt.subplot(3, 1, 1)
plt.plot(time, y, color='blue', alpha=0.7, label='Original Audio')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.legend()

# Noisy audio subplot
plt.subplot(3, 1, 2)
plt.plot(time, y_noisy, color='red', alpha=0.7, label='Noisy Audio')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.legend()

# Restored audio subplot
plt.subplot(3, 1, 3)
plt.plot(time, restored_audio, color='green', linestyle='--', label='Restored Audio')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.legend()

plt.tight_layout()
plt.show()
