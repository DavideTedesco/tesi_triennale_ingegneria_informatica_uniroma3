# Calculate Signal-to-Noise Ratio (SNR)
def calculate_snr(original_signal, restored_signal):
    power_original = np.sum(original_signal ** 2)
    power_noise = np.sum((original_signal - restored_signal) ** 2)
    snr = 10 * np.log10(power_original / power_noise)
    return snr

# Calculate Mean Squared Error (MSE)
def calculate_mse(original_signal, restored_signal):
    mse = np.mean((original_signal - restored_signal) ** 2)
    return mse

# Calculate SNR and MSE
snr_value = calculate_snr(y, restored_audio)
mse_value = calculate_mse(y, restored_audio)

print("SNR:", snr_value)
print("MSE:", mse_value)
