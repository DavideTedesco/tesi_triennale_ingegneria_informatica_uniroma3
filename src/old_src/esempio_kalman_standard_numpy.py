import numpy as np

class KalmanFilter:
    def __init__(self, F, H, Q, R, x0, P0):
        self.F = F  # Matrice di transizione di stato
        self.H = H  # Matrice di osservazione
        self.Q = Q  # Covarianza del processo
        self.R = R  # Covarianza della misura
        self.x = x0  # Stato iniziale
        self.P = P0  # Covarianza iniziale

    def predict(self):
        # Predizione dello stato successivo
        self.x = np.dot(self.F, self.x)
        # Predizione della covarianza
        self.P = np.dot(np.dot(self.F, self.P), self.F.T) + self.Q
        return self.x

    def update(self, z):
        # Calcolo del guadagno di Kalman
        S = np.dot(self.H, np.dot(self.P, self.H.T)) + self.R
        K = np.dot(np.dot(self.P, self.H.T), np.linalg.inv(S))
        # Aggiornamento dello stato
        y = z - np.dot(self.H, self.x)  # Innovazione o residuo
        self.x = self.x + np.dot(K, y)
        # Aggiornamento della covarianza
        I = np.eye(self.P.shape[0])
        self.P = np.dot((I - np.dot(K, self.H)), self.P)
        return self.x

# Parametri iniziali
F = np.array([[1, 1], [0, 1]])  # Matrice di transizione di stato (es. moto costante)
H = np.array([[1, 0]])          # Matrice di osservazione
Q = np.array([[1, 0], [0, 1]])  # Covarianza del processo
R = np.array([[1]])             # Covarianza della misura
x0 = np.array([0, 1])           # Stato iniziale (posizione e velocita)
P0 = np.eye(2)                  # Covarianza iniziale

# Creazione del filtro di Kalman
kf = KalmanFilter(F, H, Q, R, x0, P0)

# Esecuzione del filtro con nuove osservazioni
for z in [1, 2, 3]:  # Esempio di misurazioni
    pred = kf.predict()
    est = kf.update(z)
    print(f"Predizione:{pred}, Stima aggiornata:{est}")
