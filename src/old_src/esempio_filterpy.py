from filterpy.kalman import KalmanFilter
import numpy as np

# Inizializzazione del filtro di Kalman
kf = KalmanFilter(dim_x=2, dim_z=1)
kf.x = np.array([[0.], [0.]])  # Stato iniziale [posizione, velocità]
kf.F = np.array([[1., 1.], [0., 1.]])  # Matrice di transizione dello stato
kf.H = np.array([[1., 0.]])           # Matrice di osservazione
kf.P = np.eye(2) * 1000.              # Matrice di covarianza iniziale
kf.R = np.array([[5]])                # Rumore di misura
kf.Q = np.eye(2) * 0.1                # Rumore di processo

# Simulazione di aggiornamenti
misure = [1, 2, 3]  # Misure osservate
for misura in misure:
    kf.predict()
    kf.update(misura)
    print(kf.x)  # Stima aggiornata dello stato
