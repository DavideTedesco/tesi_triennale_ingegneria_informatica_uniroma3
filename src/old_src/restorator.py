import numpy as np
import librosa
import matplotlib.pyplot as plt
import os
import soundfile as sf

def process_model(x, A_q, b_q, e, w):
    p = len(w)
    x_pred = np.dot(A_q, x) + np.concatenate((b_q * e, w))
    return x_pred

def measurement_model(x, c, zeta):
    return np.dot(c, x) + zeta

def linearize_process_model(x_hat, A_q, s_p, p):
    q = len(x_hat)
    F = np.zeros((q, q))
    F[:p, :p] = A_q
    F[p:, :p] = np.eye(p)
    F[p:, p:] = np.eye(p)
    return F

def calculate_Omega(b_q, sigma_e, sigma_w):
    p = len(b_q)
    xi = sigma_w ** 2 / sigma_e ** 2
    Omega = np.zeros((p + 1, p + 1))
    Omega[0, 0] = np.dot(b_q, b_q.T)
    Omega[1:, 1:] = xi * np.eye(p)
    return Omega

def predict(x_hat_prev, Sigma_prev, F, Omega):
    x_pred = np.dot(F, x_hat_prev)
    Sigma_pred = np.dot(np.dot(F, Sigma_prev), F.T) + Omega
    return x_pred, Sigma_pred

def update(x_pred, Sigma_pred, y, c, sigma_z):
    K = np.dot(Sigma_pred, c) / (np.dot(np.dot(c, Sigma_pred), c.T) + sigma_z)
    epsilon = y - np.dot(c, x_pred)
    x_update = x_pred + np.dot(K, epsilon)
    Sigma_update = np.dot(np.eye(len(x_pred)) - np.outer(K, c), Sigma_pred)
    return x_update, Sigma_update

def kalman_filter_audio_restoration(y_noisy, sigma_e, sigma_w, sigma_z, sr):
    T = len(y_noisy)
    e = np.random.normal(0, sigma_e, T)
    w = np.random.normal(0, sigma_w, T)
    time = librosa.times_like(y_noisy, sr=sr)

    p = 1
    A_q = np.array([[0.9]])
    b_q = np.array([1.0])
    Omega = calculate_Omega(b_q, sigma_e, sigma_w)

    x_hat = np.zeros((p + 1, 1))
    Sigma = np.eye(p + 1)
    restored_audio = []

    for t in range(T):
        F = linearize_process_model(x_hat, A_q, x_hat[:p], p)
        x_pred, Sigma_pred = predict(x_hat, Sigma, F, Omega)

        y_pred = measurement_model(x_pred, np.array([1]), 0)
        x_hat, Sigma = update(x_pred, Sigma_pred, y_noisy[t], np.array([1]), sigma_z)
        restored_audio.append(y_pred[0])

    restored_audio = np.array(restored_audio).flatten()
    return restored_audio, time

# Parametri
audio_path = 'drumsMlp.mp3'
y, sr = librosa.load(audio_path)
sigma_e = 0.5
sigma_w = 0.2
sigma_z = 0.1

# Aggiungi rumore per simulare il segnale degradato
z = np.random.normal(0, sigma_z, len(y))
y_noisy = y + z

# Applica il filtro di Kalman per il restauro
restored_audio, time = kalman_filter_audio_restoration(y_noisy, sigma_e, sigma_w, sigma_z, sr)

# Visualizza il segnale originale, degradato e restaurato
plt.figure(figsize=(12, 8))
plt.subplot(3, 1, 1)
plt.plot(time, y, color='blue', alpha=0.7, label='Audio Originale')
plt.xlabel('Tempo (s)')
plt.ylabel('Ampiezza')
plt.legend()

plt.subplot(3, 1, 2)
plt.plot(time, y_noisy, color='red', alpha=0.7, label='Audio Degradato')
plt.xlabel('Tempo (s)')
plt.ylabel('Ampiezza')
plt.legend()

plt.subplot(3, 1, 3)
plt.plot(time, restored_audio, color='green', linestyle='--', label='Audio Restaurato')
plt.xlabel('Tempo (s)')
plt.ylabel('Ampiezza')
plt.legend()
plt.tight_layout()
plt.show()

# Esporta l'audio restaurato con suffisso "_restaurato"
file_name, file_extension = os.path.splitext(audio_path)
restored_audio_path = f"{file_name}_restaurato{file_extension}"
sf.write(restored_audio_path, restored_audio, sr)
print(f"File audio restaurato salvato come: {restored_audio_path}")
