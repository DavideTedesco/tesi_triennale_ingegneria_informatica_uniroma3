import numpy as np

# Define the process and measurement models
def process_model(x, A_q, b_q, e, w):
    """
    Process model function

    Parameters:
        x (numpy array): State vector at time t
        A_q (numpy array): Companion matrix associated with extended parameter vector a_q
        b_q (numpy array): Coefficient vector for the noise term
        e (float): White noise sequence e(t)
        w (numpy array): White noise sequence w(t)

    Returns:
        numpy array: Predicted state vector at time t+1
    """
    p = len(w)
    x_pred = np.dot(A_q, x) + np.concatenate((b_q * e, w))
    return x_pred

def measurement_model(x, c, zeta):
    """
    Measurement model function

    Parameters:
        x (numpy array): State vector at time t
        c (numpy array): Measurement coefficient vector
        zeta (float): Combined noise term

    Returns:
        float: Measured output at time t
    """
    return np.dot(c, x) + zeta

def linearize_process_model(x_hat, A_q, s_p, p):
    """
    Linearization of the process model

    Parameters:
        x_hat (numpy array): Filtered state trajectory (x^T(t|t))
        A_q (numpy array): Companion matrix associated with extended parameter vector a_q
        s_p (numpy array): Vector composed of the first p components of s_q(t|t)
        p (int): AR model order

    Returns:
        numpy array: Jacobian matrix representing the linearized process model (F)
    """
    q = len(x_hat)
    F = np.zeros((q, q))  # Initialize F as a square matrix with dimensions (q, q)
    F[:p, :p] = A_q  # Assign A_q to the upper-left block of F
    F[p:, :p] = np.eye(p)  # Identity matrix for the lower-left block of F
    F[p:, p:] = np.eye(p)  # Identity matrix for the lower-right block of F
    return F


def calculate_Omega(b_q, sigma_e, sigma_w):
    """
    Calculate the covariance matrix Omega

    Parameters:
        b_q (numpy array): Coefficient vector for the noise term
        sigma_e (float): Variance of white noise sequence e(t)
        sigma_w (float): Variance of white noise sequence w(t)

    Returns:
        numpy array: Covariance matrix Omega
    """
    p = len(b_q)
    xi = sigma_w ** 2 / sigma_e ** 2
    Omega = np.zeros((p + 1, p + 1))
    Omega[0, 0] = np.dot(b_q, b_q.T)
    Omega[1:, 1:] = xi * np.eye(p)
    return Omega


def predict(x_hat_prev, Sigma_prev, F, Omega):
    """
    Perform the prediction step of the Kalman filter.

    Parameters:
        x_hat_prev (numpy array): Previous state estimate (x_hat(t|t-1))
        Sigma_prev (numpy array): Previous state covariance estimate (Sigma(t|t-1))
        F (numpy array): Jacobian matrix representing the linearized process model
        Omega (numpy array): Covariance matrix representing the process noise

    Returns:
        numpy array: Predicted state estimate (x_hat(t|t))
        numpy array: Predicted state covariance estimate (Sigma(t|t))
    """
    # Predicted state estimate
    x_pred = np.dot(F, x_hat_prev)

    # Predicted state covariance estimate
    Sigma_pred = np.dot(np.dot(F, Sigma_prev), F.T) + Omega

    return x_pred, Sigma_pred

def update(x_pred, Sigma_pred, y, c, sigma_z, d_hat):
    """
    Perform the update step of the Kalman filter.

    Parameters:
        x_pred (numpy array): Predicted state estimate (x_hat(t|t))
        Sigma_pred (numpy array): Predicted state covariance estimate (Sigma(t|t))
        y (float): Measurement at time t
        c (numpy array): Measurement matrix
        sigma_z (float): Variance of measurement noise
        d_hat (int): Click indicator function

    Returns:
        numpy array: Updated state estimate (x_hat(t|t))
        numpy array: Updated state covariance estimate (Sigma(t|t))
    """
    # Innovation
    epsilon = y - np.dot(c, x_pred)

    # Kalman gain
    if d_hat == 0:
        K = np.dot(Sigma_pred, c) / (np.dot(np.dot(c, Sigma_pred), c.T) + sigma_z)
    else:
        K = np.zeros_like(Sigma_pred)

    # Updated state estimate
    x_update = x_pred + np.dot(K, epsilon)

    # Updated state covariance estimate
    Sigma_update = np.dot(np.eye(len(x_pred)) - np.dot(K, c), Sigma_pred)

    return x_update, Sigma_update


def detect_click(epsilon, mu, sigma_epsilon_hat):
    """
    Detect clicks based on the prediction error.

    Parameters:
        epsilon (float): Prediction error at time t
        mu (float): Threshold parameter for detection of impulsive noise
        sigma_epsilon_hat (float): Estimated innovation variance

    Returns:
        int: Detected click (0 for no click, 1 for click)
    """
    if abs(epsilon) <= mu * sigma_epsilon_hat:
        return 0  # No click
    else:
        return 1  # Click detected

def update_innovation_variance(sigma_epsilon_prev, epsilon, c, Sigma_prev, k):
    """
    Update the estimated innovation variance.

    Parameters:
        sigma_epsilon_prev (float): Previous estimated innovation variance
        epsilon (float): Prediction error at time t
        c (numpy array): Measurement matrix
        Sigma_prev (numpy array): Previous state covariance estimate (Sigma(t|t-1))
        k (numpy array): Kalman gain

    Returns:
        float: Updated estimated innovation variance
    """
    # Calculate eta
    eta_t = np.dot(np.dot(c, Sigma_prev), c.T) + k

    # Update the estimated innovation variance
    if detect_click(epsilon, mu, sigma_epsilon_prev) == 0:
        sigma_epsilon_t = lambda_val * sigma_epsilon_prev + (1 - lambda_val) * (epsilon ** 2 / eta_t)
    else:
        sigma_epsilon_t = sigma_epsilon_prev

    return sigma_epsilon_t


def ewls_update(x_hat_prev, Sigma_prev, epsilon, c, gamma):
    """
    Perform the update step of the Exponentially Weighted Least Squares (EWLS) algorithm.

    Parameters:
        x_hat_prev (numpy array): Previous state estimate (x_hat(t|t-1))
        Sigma_prev (numpy array): Previous state covariance estimate (Sigma(t|t-1))
        epsilon (float): Prediction error (Kalman filter innovation) at time t
        c (numpy array): Measurement matrix
        gamma (float): EWLS parameter (0 < gamma < 1)

    Returns:
        numpy array: Updated state estimate (x_hat(t|t))
        numpy array: Updated state covariance estimate (Sigma(t|t))
    """
    # Kalman gain for EWLS
    L = np.dot(Sigma_prev, c) / (np.dot(np.dot(c, Sigma_prev), c.T))

    # Updated state estimate for EWLS
    x_update = x_hat_prev + np.dot(L, epsilon)

    # Updated covariance estimate for EWLS
    Sigma_update = (1 / gamma) * (Sigma_prev - np.outer(L, np.dot(Sigma_prev, c.T)))

    return x_update, Sigma_update
