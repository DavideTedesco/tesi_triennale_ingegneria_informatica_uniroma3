import numpy as np
import scipy.io.wavfile
import soundfile as sf

def calculate_Omega(b_q, sigma_e, sigma_w):
    p = len(b_q)
    xi = (sigma_w ** 2) / (sigma_e ** 2)
    Omega = np.zeros((p, p))
    Omega[:p, :p] = np.outer(b_q, b_q) + xi * np.eye(p)
    return Omega

def update(x_pred, P_pred, y, H, R):
    S = np.dot(H, np.dot(P_pred, H.T)) + R  # Innovazione
    K = np.dot(P_pred, np.dot(H.T, np.linalg.inv(S)))  # Guadagno di Kalman
    x_update = x_pred + np.dot(K, (y - np.dot(H, x_pred)))  # Stato aggiornato
    P_update = P_pred - np.dot(K, np.dot(H, P_pred))  # Covarianza aggiornata
    return x_update, P_update

def kalman_filter_audio_restoration(y_noisy, A_q, b_q, sigma_e, sigma_w, H, R, sample_rate):
    n_samples = len(y_noisy)
    p = len(b_q)
    x = np.zeros((n_samples, p))
    P = np.eye(p)

    for t in range(n_samples):
        y_t = y_noisy[t]

        # Predizione
        x_pred = np.dot(A_q, x[t - 1]) if t > 0 else np.zeros(p)
        P_pred = np.dot(A_q, np.dot(P, A_q.T)) + calculate_Omega(b_q, sigma_e, sigma_w)

        # Aggiornamento
        x[t], P = update(x_pred, P_pred, y_t, H, R)

    return x

def main():
    sample_rate = 44100
    A_q = np.array([[0.95]])
    b_q = np.array([1])
    sigma_e = 0.7
    sigma_w = 0.7
    R = np.array([[0.1]])  # Rumore di misura
    H = np.array([[1]])    # Modello di misura

    _, y_noisy = scipy.io.wavfile.read('./vynil.wav')
    if len(y_noisy.shape) > 1:  # Converti in mono se il file è stereo
        y_noisy = y_noisy.mean(axis=1)
    y_noisy = y_noisy.astype(np.float32) / 32768.0  # Normalizzazione

    x = kalman_filter_audio_restoration(y_noisy, A_q, b_q, sigma_e, sigma_w, H, R, sample_rate)
    restored_audio = (x[:, 0] * 32768.0).astype(np.int16)
    sf.write('./restored_file.wav', restored_audio, sample_rate)

if __name__ == "__main__":
    main()
