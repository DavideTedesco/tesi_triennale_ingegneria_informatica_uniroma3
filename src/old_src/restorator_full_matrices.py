import numpy as np
from scipy.io.wavfile import read, write
import matplotlib.pyplot as plt

# Process Model Function
def process_model(x, A_q, b_q, e, w):
    p = len(w)
    x_pred = np.dot(A_q, x) + np.concatenate((b_q * e, w))
    return x_pred

# Measurement Model Function
def measurement_model(x, c, zeta):
    return np.dot(c, x) + zeta

# Linearization of the Process Model
def linearize_process_model(x_hat, A_q, p):
    q = len(x_hat)
    F = np.zeros((q, q))
    F[:p, :p] = A_q
    F[p:, :p] = np.eye(p)
    F[p:, p:] = np.eye(p)
    return F

# Calculate Covariance Matrix Omega
def calculate_Omega(b_q, sigma_e, sigma_w):
    p = len(b_q)
    xi = sigma_w ** 2 / sigma_e ** 2
    Omega = np.zeros((p + 1, p + 1))
    Omega[0, 0] = np.dot(b_q, b_q.T)
    Omega[1:, 1:] = xi * np.eye(p)
    return Omega

# Kalman Filter Prediction Step
def predict(x_hat_prev, Sigma_prev, F, Omega):
    x_pred = np.dot(F, x_hat_prev)
    Sigma_pred = np.dot(np.dot(F, Sigma_prev), F.T) + Omega
    return x_pred, Sigma_pred

# Kalman Filter Update Step
def update(x_pred, Sigma_pred, y, c, sigma_z, d_hat):
    epsilon = y - np.dot(c, x_pred)
    if d_hat == 0:
        K = np.dot(Sigma_pred, c) / (np.dot(np.dot(c, Sigma_pred), c.T) + sigma_z)
    else:
        K = np.zeros_like(Sigma_pred)
    x_update = x_pred + np.dot(K, epsilon)
    Sigma_update = np.dot(np.eye(len(x_pred)) - np.dot(K, c), Sigma_pred)
    return x_update, Sigma_update

# Click Detection
def detect_click(epsilon, mu, sigma_epsilon_hat):
    if abs(epsilon) <= mu * sigma_epsilon_hat:
        return 0  # No click
    else:
        return 1  # Click detected

# Update Innovation Variance
def update_innovation_variance(sigma_epsilon_prev, epsilon, c, Sigma_prev, k, lambda_val):
    eta_t = np.dot(np.dot(c, Sigma_prev), c.T) + k
    if detect_click(epsilon, mu=3, sigma_epsilon_hat=sigma_epsilon_prev) == 0:
        sigma_epsilon_t = lambda_val * sigma_epsilon_prev + (1 - lambda_val) * (epsilon ** 2 / eta_t)
    else:
        sigma_epsilon_t = sigma_epsilon_prev
    return sigma_epsilon_t

# Load an audio file (WAV format)
audio_path = 'example_audio.wav'  # Replace with your WAV file path
sr, y = read(audio_path)  # sr: sample rate, y: audio data

# Convert stereo to mono if necessary
if len(y.shape) == 2:  # Check if stereo
    y = np.mean(y, axis=1)  # Convert to mono by averaging channels

# Normalize the audio signal if it's in integer format
if y.dtype != np.float32:
    y = y / np.max(np.abs(y))

# Noise Parameters
sigma_e = 0.5
sigma_w = 0.2
sigma_z = 0.1

# Generate Noisy Signal
T = len(y)
e = np.random.normal(0, sigma_e, T)
w = np.random.normal(0, sigma_w, T)
z = np.random.normal(0, sigma_z, T)

v = np.zeros(T)
click_probability = 0.05
for t in range(T):
    if np.random.rand() < click_probability:
        v[t] = np.random.choice([-1, 1]) * np.random.uniform(10, 50)

y_noisy = y + z + v  # Add noise and clicks

# Initialize Kalman Filter Variables
q = len(y)
x_hat = np.zeros(q)
Sigma = np.eye(q)

restored_audio = []
epsilon_list = []

# Iterate Over the Signal for Restoration
for t in range(T):
    x_pred, Sigma_pred = predict(x_hat, Sigma, np.eye(q), calculate_Omega(np.ones(q), sigma_e, sigma_w))
    y_pred = measurement_model(x_pred, np.array([1]), 0)
    epsilon = y_noisy[t] - y_pred
    epsilon_list.append(epsilon)

    d_hat = detect_click(epsilon, mu=3, sigma_epsilon_hat=np.sqrt(Sigma_pred[0, 0]))
    x_update, Sigma_update = update(x_pred, Sigma_pred, y_noisy[t], np.array([1]), sigma_z, d_hat)

    restored_audio.append(y_pred)
    x_hat = x_update
    Sigma = Sigma_update

# Save the restored audio
restored_audio = np.array(restored_audio)
restored_audio = restored_audio / np.max(np.abs(restored_audio))  # Normalize
write('restored_audio.wav', sr, (restored_audio * 32767).astype(np.int16))

# Plot Results
time = np.linspace(0, len(y) / sr, num=len(y))
plt.figure(figsize=(10, 8))
plt.subplot(3, 1, 1)
plt.plot(time, y, color='blue', alpha=0.7, label='Original Audio')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.legend()

plt.subplot(3, 1, 2)
plt.plot(time, y_noisy, color='red', alpha=0.7, label='Noisy Audio')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.legend()

plt.subplot(3, 1, 3)
plt.plot(time, restored_audio, color='green', linestyle='--', label='Restored Audio')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.legend()

plt.tight_layout()
plt.show()
