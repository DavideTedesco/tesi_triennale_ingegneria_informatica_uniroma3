import numpy as np
import librosa
import matplotlib.pyplot as plt
import os
import soundfile as sf
import argparse

# Funzione del modello di processo
def process_model(x, A_q, b_q, e, w):
    print("process_model:")
    print(f" - Stato x: {x}")
    print(f" - Matrice A_q: {A_q}, b_q: {b_q}, rumore e: {e}, rumore w: {w}")

    p = len(w)
    x_pred = np.dot(A_q, x) + np.concatenate((b_q * e, w))
    print(f" - Stato predetto x_pred: {x_pred}")
    return x_pred

# Funzione del modello di misura
def measurement_model(x, c, zeta):
    print("measurement_model:")
    print(f" - Stato x: {x}")
    print(f" - Coefficiente c: {c}, Rumore zeta: {zeta}")

    result = np.dot(c, x) + zeta
    print(f" - Misura calcolata: {result}")
    return result

# Linearizzazione del modello di processo
def linearize_process_model(x_hat, A_q, p):
    print("linearize_process_model:")
    print(f" - Stato stimato x_hat: {x_hat}, Matrice A_q: {A_q}, p: {p}")

    q = len(x_hat)
    F = np.zeros((q, q))
    F[:p, :p] = A_q
    F[p:, :p] = np.eye(p)
    F[p:, p:] = np.eye(p)

    print(f" - Matrice linearizzata F: {F}")
    return F

# Calcolo della matrice di covarianza Omega
def calculate_Omega(b_q, sigma_e, sigma_w):
    print("calculate_Omega:")
    print(f" - b_q: {b_q}, sigma_e: {sigma_e}, sigma_w: {sigma_w}")

    p = len(b_q)
    xi = sigma_w ** 2 / sigma_e ** 2
    Omega = np.zeros((p, p))  # Modificato da (p+1, p+1) a (p, p)

    # Qui puoi aggiungere logica per popolare Omega, se necessario
    print(f" - Matrice Omega (dimensioni corrette): {Omega.shape}")
    return Omega


# Passo di predizione del filtro di Kalman
def predict(x_hat_prev, Sigma_prev, F, Omega):
    print("predict")
    print(f" - Stato precedente x_hat_prev: {x_hat_prev}")
    print(f" - Matrice di covarianza Sigma_prev: {Sigma_prev}")
    print(f" - Matrice F: {F}, Omega: {Omega}")
    print(f" - F: {F.shape}, Sigma_prev: {Sigma_prev.shape}, Omega: {Omega.shape}")

    x_pred = np.dot(F, x_hat_prev)
    Sigma_pred = np.dot(np.dot(F, Sigma_prev), F.T) + Omega

    print(f" - Stato predetto x_pred: {x_pred}")
    print(f" - Covarianza predetta Sigma_pred: {Sigma_pred}")
    return x_pred, Sigma_pred

# Passo di aggiornamento del filtro di Kalman
def update(x_pred, Sigma_pred, y, c, sigma_z, d_hat):
    print("update:")
    print(f" - Stato predetto x_pred: {x_pred}, Sigma_pred: {Sigma_pred}")
    print(f" - Misura y: {y}, Coefficiente c: {c}, sigma_z: {sigma_z}, d_hat: {d_hat}")

    # Assicurati che `c` sia un vettore colonna
    c = c.reshape(-1, 1)  # Trasforma `c` in un vettore colonna se non lo è già
    print(f" - Dimensione adattata c: {c.shape}")

    # Calcolo del guadagno di Kalman
    S = np.dot(c.T, np.dot(Sigma_pred, c)) + sigma_z  # Scala
    K = np.dot(Sigma_pred, c) / S  # Vettore colonna (2205, 1)
    print(f" - Guadagno di Kalman K: {K.shape}")

    # Calcolo dell'innovazione
    epsilon = y - float(np.dot(c.T, x_pred))
    print(f" - Innovazione epsilon (scalare): {epsilon}")

    # Aggiorna lo stato solo se il click è rilevato
    if d_hat:
        x_update = x_pred + K.flatten() * epsilon
        print(" - Stato aggiornato (click rilevato): eseguito update dello stato.")
    else:
        x_update = x_pred
        print(" - Stato aggiornato (click non rilevato): mantenuto lo stato predetto.")

    # Aggiorna la covarianza
    Sigma_update = np.dot(np.eye(len(x_pred)) - np.dot(K, c.T), Sigma_pred)
    print(f" - Dimensione Sigma_update: {Sigma_update.shape}")

    return x_update, Sigma_update

# Rilevamento dei click
def detect_click(epsilon, mu, sigma_epsilon_hat):
    print("detect_click:")
    print(f" - Innovazione epsilon: {epsilon}, mu: {mu}, sigma_epsilon_hat: {sigma_epsilon_hat}")

    return abs(epsilon) <= mu * sigma_epsilon_hat


# Aggiornamento della varianza dell'innovazione
def update_innovation_variance(sigma_epsilon_prev, epsilon, c, Sigma_prev, k, lambda_val):
    print("update_innovation_variance:")
    print(f" - sigma_epsilon_prev: {sigma_epsilon_prev}, epsilon: {epsilon}, c: {c}")
    print(f" - Sigma_prev: {Sigma_prev}, k: {k}, lambda_val: {lambda_val}")

    eta_t = np.dot(np.dot(c, Sigma_prev), c.T) + k
    if detect_click(epsilon, mu=3, sigma_epsilon_hat=sigma_epsilon_prev) == 0:
        sigma_epsilon_t = lambda_val * sigma_epsilon_prev + (1 - lambda_val) * (epsilon ** 2 / eta_t)
    else:
        sigma_epsilon_t = sigma_epsilon_prev

    print(f" - Nuova varianza dell'innovazione sigma_epsilon_t: {sigma_epsilon_t}")
    return sigma_epsilon_t

# Funzione principale che esegue il programma
def main(audio_path):
    # Carica l'audio dal percorso specificato
    print(f"Caricamento audio da: {audio_path}")
    y, sr = librosa.load(audio_path)

    # Parametri del rumore
    sigma_e = 0.5
    sigma_w = 0.2
    sigma_z = 0.1

    # Generazione del segnale rumoroso
    T = len(y)
    e = np.random.normal(0, sigma_e, T)
    w = np.random.normal(0, sigma_w, T)
    z = np.random.normal(0, sigma_z, T)

    v = np.zeros(T)
    click_probability = 0.05
    for t in range(T):
        if np.random.rand() < click_probability:
            v[t] = np.random.choice([-1, 1]) * np.random.uniform(10, 50)

    y_noisy = y + z + v

    # Inizializzazione delle variabili del filtro di Kalman
    q = len(y)
    x_hat = np.zeros(q)
    Sigma = np.eye(q)

    restored_audio = []
    epsilon_list = []

    # Iterazione sul segnale per la restaurazione
    for t in range(T):
        print(f"\nIterazione {t + 1} di {T}:")
        c = np.ones(q)  # Adattamento: vettore di dimensione q
        x_pred, Sigma_pred = predict(x_hat, Sigma, np.eye(q), calculate_Omega(np.ones(q), sigma_e, sigma_w))
        y_pred = measurement_model(x_pred, np.ones_like(x_pred), 0)
        epsilon = y_noisy[t] - y_pred
        epsilon_list.append(epsilon)

        d_hat = detect_click(epsilon, mu=3, sigma_epsilon_hat=np.sqrt(Sigma_pred[0, 0]))
        x_update, Sigma_update = update(x_pred, Sigma_pred, y_noisy[t], c, sigma_z, d_hat)
        restored_audio.append(y_pred)
        x_hat = x_update
        Sigma = Sigma_update
    # Salvataggio del file audio restaurato
    base_name, ext = os.path.splitext(audio_path)  # Estrai nome e estensione
    output_path = f"{base_name}_restored{ext}"  # Crea il nome del file output
    sf.write(output_path, np.array(restored_audio), sr)  # Salva il file audio
    print(f"File audio restaurato salvato come: {output_path}")

    # Tracciamento dei risultati
    time = librosa.times_like(y, sr=sr)
    plt.figure(figsize=(10, 8))
    plt.subplot(3, 1, 1)
    plt.plot(time, y, color='blue', alpha=0.7, label='Audio originale')
    plt.xlabel('Tempo (s)')
    plt.ylabel('Ampiezza')
    plt.legend()

    plt.subplot(3, 1, 2)
    plt.plot(time, y_noisy, color='red', alpha=0.7, label='Audio rumoroso')
    plt.xlabel('Tempo (s)')
    plt.ylabel('Ampiezza')
    plt.legend()

    plt.subplot(3, 1, 3)
    plt.plot(time, restored_audio, color='green', linestyle='--', label='Audio restaurato')
    plt.xlabel('Tempo (s)')
    plt.ylabel('Ampiezza')
    plt.legend()

    plt.tight_layout()

    # Salvataggio del plot come immagine
    plot_filename = f"{base_name}_restored_plot.png"
    plt.savefig(plot_filename)
    print(f"Plot salvato come: {plot_filename}")

    # Visualizza il plot
    plt.show()

    # Chiudi il plot per evitare conflitti con altri plot
    plt.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Restauratore audio usando il filtro di Kalman.")
    parser.add_argument("audio_path", type=str, help="Percorso del file audio da restaurare")
    args = parser.parse_args()

    # Esegui la funzione principale con il percorso del file audio passato come argomento
    main(args.audio_path)
