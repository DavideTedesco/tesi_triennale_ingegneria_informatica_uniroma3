import numpy as np
import librosa
import matplotlib.pyplot as plt
import os
import soundfile as sf
import argparse

# Funzione principale che esegue il programma
def main(audio_path):
    # Carica l'audio dal percorso specificato
    print(f"Caricamento audio da: {audio_path}")
    y, sr = librosa.load(audio_path)

    base_name, ext = os.path.splitext(audio_path)  # Estrai nome e estensione

    # Calcolo dello sonogramma
    S = librosa.feature.melspectrogram(y=y, sr=sr, n_mels=128, fmax=8000)
    S_dB = librosa.power_to_db(S, ref=np.max)

    # Tracciamento dei risultati
    time = librosa.times_like(y, sr=sr)
    plt.figure(figsize=(15, 8))
    plt.subplot(2, 1, 1)
    plt.title('Forma d\'onda')
    plt.plot(time*2, y, color='blue', alpha=0.7)
    plt.xlabel('Tempo (ms)')
    plt.ylabel('Ampiezza')


    plt.subplot(2, 1, 2)
    librosa.display.specshow(S_dB, sr=sr, x_axis='time', y_axis='mel', fmax=20000, cmap='coolwarm')
    plt.colorbar(format='%+2.0f dB')
    plt.title('Sonogramma')
    plt.xlabel('Tempo (ms)')
    plt.ylabel('Frequenza (Hz)')


    plt.tight_layout()

    # Salvataggio del plot come immagine
    plot_filename = f"{base_name}_restored_plot.png"
    plt.savefig(plot_filename)
    print(f"Plot salvato come: {plot_filename}")

    # Visualizza il plot
    plt.show()

    # Chiudi il plot per evitare conflitti con altri plot
    plt.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Visualizzatore della forma d'onda di un file audio restaurato e del suo sonogramma.")
    parser.add_argument("audio_path", type=str, help="Percorso del file audio restaurato")
    args = parser.parse_args()

    # Esegui la funzione principale con il percorso del file audio passato come argomento
    main(args.audio_path)
