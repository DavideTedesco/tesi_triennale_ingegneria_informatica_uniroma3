import numpy as np
import librosa
import matplotlib.pyplot as plt
import os
import soundfile as sf
import argparse

# Funzione principale che esegue il programma
def main(audio_path):
    # Carica l'audio dal percorso specificato
    print(f"Caricamento audio da: {audio_path}")
    y, sr = librosa.load(audio_path)

    # Parametri del rumore
    sigma_e = 0.5
    sigma_w = 0.2
    sigma_z = 0.1

    # Generazione del segnale rumoroso
    T = len(y)
    e = np.random.normal(0, sigma_e, T)
    w = np.random.normal(0, sigma_w, T)
    z = np.random.normal(0, sigma_z, T)

    v = np.zeros(T)
    click_probability = 0.05
    for t in range(T):
        if np.random.rand() < click_probability:
            v[t] = np.random.choice([-1, 1]) * np.random.uniform(10, 50)

    y_noisy = y + z + v

    base_name, ext = os.path.splitext(audio_path)  # Estrai nome e estensione

    # Tracciamento dei risultati
    time = librosa.times_like(y, sr=sr)
    plt.figure(figsize=(10, 8))
    plt.subplot(2, 1, 1)
    plt.plot(time, y, color='blue', alpha=0.7, label='Audio originale')
    plt.xlabel('Tempo (ms)')
    plt.ylabel('Ampiezza')
    plt.legend()

    plt.subplot(2, 1, 2)
    plt.plot(time, y_noisy, color='red', alpha=0.7, label='Audio rumoroso')
    plt.xlabel('Tempo (ms)')
    plt.ylabel('Ampiezza')
    plt.legend()

    plt.tight_layout()

    # Salvataggio del plot come immagine
    plot_filename = f"{base_name}_noisy_plot.png"
    plt.savefig(plot_filename)
    print(f"Plot salvato come: {plot_filename}")

    # Visualizza il plot
    plt.show()

    # Chiudi il plot per evitare conflitti con altri plot
    plt.close()

    # Salvataggio dell'audio rumoroso
    noisy_audio_filename = f"{base_name}_noisy.wav"
    sf.write(noisy_audio_filename, y_noisy, sr)
    print(f"Audio rumoroso salvato come: {noisy_audio_filename}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Visualizzatore di un file audio originale e delle stesso file aggiunta di rumore.")
    parser.add_argument("audio_path", type=str, help="Percorso del file audio da restaurare")
    args = parser.parse_args()

    # Esegui la funzione principale con il percorso del file audio passato come argomento
    main(args.audio_path)
