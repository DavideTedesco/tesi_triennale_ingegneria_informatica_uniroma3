import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, spectrogram

# Impostazioni
fs = 96000  # Frequenza di campionamento in Hz
duration = 1.0  # Durata in secondi
cutoff = 5000  # Frequenza di taglio per il filtro passa basso in Hz

# Generazione di rumore bianco
t = np.linspace(0, duration, int(fs * duration), endpoint=False)
white_noise = np.random.normal(0, 1, t.shape)

# Creazione del filtro passa-basso
def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs  # Frequenza di Nyquist
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

# Applicazione del filtro al rumore bianco
filtered_noise = lowpass_filter(white_noise, cutoff, fs)

# Plot della forma d'onda e sonogramma del rumore bianco e rumore bianco filtrato
fig, axs = plt.subplots(2, 2, figsize=(12, 8))

# Rumore bianco - forma d'onda
axs[0, 0].plot(t, white_noise)
axs[0, 0].set_title("Rumore bianco - Forma d'onda")
axs[0, 0].set_xlabel("Tempo [s]")
axs[0, 0].set_ylabel("Ampiezza [dB]")

# Rumore bianco - sonogramma
frequencies, times, Sxx = spectrogram(white_noise, fs)
axs[1, 0].pcolormesh(times, frequencies, 10 * np.log10(Sxx), shading='gouraud')
axs[1, 0].set_title("Rumore bianco - Sonogramma")
axs[1, 0].set_xlabel("Tempo [s]")
axs[1, 0].set_ylabel("Frequenza [Hz]")
axs[1, 0].set_ylim(0, fs / 2)

# Rumore bianco filtrato - forma d'onda
axs[0, 1].plot(t, filtered_noise)
axs[0, 1].set_title("Rumore bianco filtrato (passa basso 5000 Hz) - Forma d'onda")
axs[0, 1].set_xlabel("Tempo [s]")
axs[0, 1].set_ylabel("Ampiezza [dB]")

# Rumore bianco filtrato - sonogramma
frequencies, times, Sxx = spectrogram(filtered_noise, fs)
axs[1, 1].pcolormesh(times, frequencies, 10 * np.log10(Sxx), shading='gouraud')
axs[1, 1].set_title("Rumore bianco filtrato (passa basso 5000 Hz) - Sonogramma")
axs[1, 1].set_xlabel("Tempo [s]")
axs[1, 1].set_ylabel("Frequenza [Hz]")
axs[1, 1].set_ylim(0, fs / 2)

# Aggiustamenti e visualizzazione
plt.tight_layout()
plt.show()
