%!TEX TS-program = latex
%!TEX encoding = UTF-8 Unicode
% !TEX root =   Davide_Tedesco-Tesi.tex

%(linguaggi di programmazione e librerie)

%https://david.wf/kalmanfilter/


Nel capitolo seguente andremo ad osservare alcune delle implementazioni più dirette, popolari ed immediate del filtro di Kalman.
Sono stati scelti i linguaggi di programmazione qui riportati solamente per un maggiore utilizzo degli stessi in ambito scientifico e tecnologico, oltre alla loro facile reperibilità e disponibilità in tali contesti.

\section{MATLAB \& Simulink}

Il nome MATLAB~\cite{MATLAB} è acronimo di MATrix LABoratory esso è originariamente stato sviluppato come ambiente interattivo per il calcolo matriciale ad alto livello. La principale caratteristica di MATLAB è che non opera con numeri, ma con matrici, possiamo infatti pensare ai vettori o a dei numeri come casi particolari di matrici.
Per via della sua versatilità esso è usato spesso come linguaggio di programmazione, calcolatrice scientifica ed ambiente grafico per la simulazione e la prototipazione. Esso è oramai da molti anni corredato dall'ambiente di programmazione Simulink~\cite{simulink}, che opera attraverso la composizione di diagrammi a blocchi ed aiuta i professionisti nella progettazione di simulazioni che si avvicinano a quelle che potrebbero essere realizzate direttamente su hardware, esso permette infatti di generare codice C, C++, CUDA, PLC, Verilog e VHDL in grado di essere utilizzato direttamente sul campo di applicazione.
Non ci soffermiamo a lungo sull'introduzione a tali linguaggi, per concentrarci sull'applicazione e l'utilizzo del filtro di Kalman possibile per mezzo di essi\footnote{N.b. la documentazione utilizzata per la scrittura di questa tesi si riferisce alla versione R2024b di MATLAB e la corrispondente versione di Simulink.}.

\subsection{MATLAB}

In MATLAB il filtro di Kalman è realizzabile per mezzo del comando \lstinline|kalman| presente nel \textit{Control System Toolbox}~\cite{ControlSystemToolbox, kalmanMATLAB}, questa funzione restituisce le matrici del filtro (o del predittore) di Kalman che possono essere utilizzate per costruire uno stimatore lineare a tempo discreto.
Un esempio di utilizzo è:
\begin{lstlisting}[language=MATLAB]
[kalman_filter, L, P] = kalman(sistema, Q, R, N);
\end{lstlisting} 

Dove \verb|Q|, \verb|R| ed \verb|N| sono le matrici di covarianza del rumore di processo e quella di misurazione, tale tipologia di filtro rispecchia lo schema:
\begin{figure}[hbt!]
\centering
\includegraphics[width=8cm]{figure/kalman_MATLAB_block_upscayl_4x_realesrgan-x4plus.png}
\caption{Schema di funzionamento della funzione \lstinline|kalman| del \textit{Control System Toolbox}~\cite{ControlSystemToolbox, kalmanMATLAB} di MATLAB.}
\end{figure}

Tale funzione, realizza un filtro di Kalman canonico e come è possibile osservare dallo schema, ci fornisce i valori predetti dell'uscita del sistema \(\hat y\) e quelli predetti dello stato del sistema \(\hat x\) oltre a fornirci i guadagni di Kalman \(L\) e l'errore di covarianza \(P\) sotto forma di matrici.

Oltre a questa versione basilare vi sono altre tipologie di funzioni in MATLAB come: \lstinline|unscentedKalmanFilter| e \lstinline|extendedKalmanFilter| che implementano rispettivamente le tipologie di filtri di Kalman osservate nelle sezioni \ref{se:ekf-spiegazione}, \ref{se:ukf-spiegazione}.

A completare le funzionalità e la capacità di tali algoritmi, vi sono anche altre tipologie di filtri di Kalman per applicazioni specifiche, come \lstinline|trackingKF|, utile per la stima di stato di oggetti in movimento e molte altre. Basta una rapida visita sul  motore di ricerca di Mathworks~\cite{motoreRicercaMathworks} per rendersi conto della vastità di utilizzazioni ed applicazioni possibili e già sviluppate.


\subsection{Simulink}
A completare l'offerta di MATLAB\footnote{Ricordando che essa è una società privata e quindi fornisce prodotti ai propri clienti.}, osserviamo in maniera rapida le realizzazioni sviluppate in Simulink.
Nel Control System Toolbox e nel System Identification Toolbox è presente un blocco che implementa il filtro di Kalman denominato \textit{Kalman Filter}~\cite{kalmanfiltersimulink} che effettua la stima del segnale \(\hat x\) in ingresso ad esso conoscendo l'ingresso \(u\) e l'uscita \(y\) del sistema in esame.
%https://it.mathworks.com/help/control/ug/state-estimation-using-time-varying-kalman-filter.html
\begin{figure}[hbt!]
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=1.5cm]{figure/icon_kalman_filter_block_upscayl_4x_realesrgan-x4plus.png}
\subcaption{Blocco \textit{Kalman Filter} del Control System Toolbox e del System Identification Toolbox.}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
%\begin{frame}
\centering
%https://www.latex4technics.com/?note=XQF
\begin{tcolorbox}[beamer,
                  width=0.19\textheight,
                  arc=0pt,
                  boxsep=0pt,
                  left=0pt,right=0pt,top=0pt,bottom=0pt,
                  ]
\includegraphics[width=4cm]{figure/kalmanfiltericon_upscayl_4x_realesrgan-x4plus.png}
%\end{frame}
\end{tcolorbox}
\subcaption{Blocco \textit{Kalman Filter} del DSP System Toolbox.}
\end{subfigure}
\caption{Le due tipologie di blocchi in Simulink per la realizzazione di filtri di Kalman.}
\end{figure}

Notiamo i blocchi dei due diversi \textit{Toolboxes}, osservando che nel caso della realizzazione per l'elaborazione dei segnali (a destra), sono presenti in uscita ad esso più parametri, e non solamente la predizione realizzata dal filtro (come in quello di destra denominata \lstinline|xhat|). Nella realizzazione del \textit{DSP System Toolbox} abbiamo la misura stimata \lstinline|Z_est|, lo stato di uscita stimato \lstinline|X_est| e la media quadratica dell'errore di stato stimato \lstinline|MSE_est|, analogamente vi sono per gli stessi valori disponibili le predizioni~\cite{kalmanSimulinkDSP}.

Senza dilungarci molto sulle varie implementazioni tramite blocchi in Simulink, osserviamo la presenza del blocco che utilizza l'algoritmo del filtro esteso di Kalman (EKF) presente nel Control System Toolbox e nel System Identification Toolbox applicabile a sistemi non lineari~\cite{ekfSimulink}.

\begin{figure}[hbt!]
\centering
\includegraphics[width=2cm]{figure/icon_extended_kalman_filter_block_upscayl_12x_realesrgan-x4plus.png}
\caption{Blocco di Simulink per la realizzazione di un filtro di Kalman esteso.}
\end{figure}
\newpage

Che ricordiamo avere il funzionamento descritto dal seguente diagramma:

\begin{figure}[hbt!]
\centering
\includegraphics[width=8cm]{figure/extendedkalmanfilter_upscayl_5x_realesrgan-x4plus.png}
\caption{Schema di MATLAB del funzionamento del filtro di Kalman esteso.}
\end{figure}
Esso esplicita quanto visto sino ad ora per il funzionamento di un filtro di kalman esteso, per il quale ricordiamo che \(u\) è l'ingresso al sistema non lineare, \(w\) è il rumore di processo, \(y\) è l'uscita del sistema non lineare a cui è stato aggiunto il rumore di misura \(v\), \(x\) non riportato nello schema (poiché non è un dato disponibile per noi, è proprio di esso che vogliamo realizzare la stima) è lo stato del sistema  e \(\hat x\) è la stima per un certo istante di tempo (ricordiamo che siamo a tempo discreto) dello stato del sistema.
%capire se inserire altro
%\section{Tipologie di implementazioni per la realizzazione di filtri} % A cosa si riferisce questa sezione?
\section{Octave}
Oltre a MATLAB, è importante nel mondo software trovare ed utilizzare alternative Open Source che ci aiutino a realizzare progetti innovativi rimanendo svincolati da società che realizzano e mantengono il loro software.

Negli anni si è affermato il software (e linguaggio di programmazione) chiamato Octave~\cite{octaveOctave}. Esso, oltre ad avere una sintassi simile a quella di MATLAB\footnote{Molti degli script realizzati in MATLAB sono direttamente compatibili con Octave.} è multipiattaforma, permette la realizzazione di grafici ed è gratuito.

Senza dilungarci oltre sull'introduzione ad Octave, andiamo subito ad osservare le funzioni di libreria che realizzano il filtro di Kalman.
All'interno del pacchetto \verb|control| troviamo la funzione \verb|kalman|~\cite{sourceforgeFunctionReferenceOctavaKalman}, essa analogamente alla funzione \lstinline|kalman| di MATLAB, permette di implementare un filtro di kalman a tempo discreto i quali input sono: il sistema, la matrice di covarianza del rumore di processo e quella del rumore di misura (oltre ad altri parametri opzionali). In uscita tale funzione fornisce: il filtro di Kalman rappresentato nella configurazione spazio di stato, una matrice con i valori dei guadagni di Kalman\footnote{Tale matrice riporta un guadagno che permette di comprendere quanto peso hanno le misurazioni realizzate rispetto alle stime calcolate dal filtro di Kalman.} e la matrice di covarianza dello stato stimato (ovvero l’incertezza associata alla stima dello stato del sistema)\footnote{Non viene riportato in questa sezione un diagramma a blocchi per la funzione di Octave, poiché esso è identico a quello visto per MATLAB.}.

Per quanto riguarda il filtro esteso di Kalman, ne esistono diverse realizzazioni da parte della community di Octave\footnote{Molto attiva a livello globale.}, come ad esempio \textit{EKF/UKF Toolbox for Matlab}~\cite{githubGitHubEEAsensorsekfukf} pensato per MATLAB, ma funzionante anche in Octave\footnote{Proprio per via della compatibilità che descrivevamo precedentemente.} ed ancora ciò che si può trovare tra le molte risorse di Stanislav N Zonov~\cite{githubGitHubMannyrayKalmanFilter} realizzatore del sito web \href{http://kalmanfilter.org}{kalmanfilter.org}.


\section{Scilab ed Xcos}
%accennare a scilab e xcos

Dopo aver osservato a grandi linee il funzionamento di Octave come alternativa Open Source a MATLAB, potremo domandarci se esista anche un'alternativa a Simulink. Tale dubbio viene istantaneamente fugato introducendo Scilab~\cite{scilabScilab}, ovvero un software orientato all'analisi numerica, elaborazione di grafici e simulazione. Proprio per quest'ultimo utilizzo viene d'aiuto \textit{Xcos}~\cite{scilabXcosScilab} che è di fatto un vero e proprio strumento di modellazione e simulazione di sistemi dinamici, incluso direttamente in Scilab. Come per Simulink, si ha in \textit{Xcos} un'interfaccia basata su blocchi grafici che rappresentano le varie componenti del sistema. Si modellano attraverso di essa (ed alle sue librerie) le varie tipologie di sistemi realizzabili, essendo però (come Octave) un software gratuito ed Open Source.

\begin{figure}[hbt!]
\centering
\includegraphics[width=8cm]{figure/kalman-filter--scilab-xcos_upscayl_4x_realesrgan-x4plus.png}
\caption{Esempio di utilizzo di \textit{Xcos} per la realizzazione di un filtro di Kalman.}
\end{figure}

\newpage

\section{Python}\label{se:python}

%Perché utilizzare Python

Dopo aver osservato tipologie di software e linguaggi di programmazione diversi, ci soffermiamo sullo strumento utilizzato per la trattazione e la realizzazione pratica del filtro di Kalman che è stata scelta in questo lavoro di tesi. Python~\cite{python} combina le caratteristiche dell'Open Source\footnote{Anche se bisogna essere molto attenti a selezionare le librerie adeguate che non abbiamo sezioni proprietarie.} con la versatilità, ed il comune utilizzo di tale strumento in ambito scientifico e tecnologico.

Attraverso una serie di librerie dalle più variegate funzionalità, è possibile realizzare (in maniera abbastanza veloce) un'applicazione all'audio diretta e veloce. Inoltre Python consente di avere una maggiore possibilità per la gestione e la realizzazione di interfacce grafiche realizzate ad hoc.

Alle motivazioni appena espresse si lega inoltre una "leggibilità" del codice maggiore rispetto ad altri linguaggi di programmazione, inoltre le numerose librerie scientifiche già menzionate, permettono di realizzare una moltitudine di versatili applicazioni che spaziano dal semplice calcolo numerico, all'analisi dei dati, fino ad arrivare all'intelligenza artificiale.

Come per tutti i progetti Open Source, di maggior successo, Python si fonda su una comunità molto attiva, permettendo l'usabilità dello stesso linguaggio di programmazione su molteplici sistemi operativi, e su macchine che non forniscono molte risorse computazionali; il che lo rende valido per vari ambiti di studio.

Proseguiamo l'approfondimento su Python, introducendo le librerie che verranno utilizzate per il caso di studio e la realizzazione in esame osservate nel quinto capitolo.

\subsection{\lstinline|filterpy|~\cite{githubGitHubRlabbefilterpy}}

Essa è una libreria di Python progettata per lavorare con filtri bayesiani, in particolare il filtro di Kalman e le sue varianti, è comunemente utilizzata in applicazioni che richiedono la stima e il filtraggio dei segnali, come la robotica, il controllo di droni, la navigazione GPS, la finanza e l'elaborazione dei segnali. La libreria fornisce strumenti e classi per lavorare con diversi tipi di filtri statistici e algoritmi di ottimizzazione. 

All'interno della libreria possiamo trovare:
\begin{itemize}
\item{Filtri di Kalman e varianti\footnote{La libreria implementa il filtro di Kalman standard, il filtro di Kalman esteso (EKF), il filtro di Kalman unscented (UKF) ed altre tipologie di filtri di Kalman.}.}
\item{Filtri di particelle\footnote{Ovvero delle tipologie di filtri utili per risolvere problemi altamente non lineari o non gaussiani. Questi filtri utilizzano tecniche basate su simulazioni Monte Carlo per stimare distribuzioni di probabilità.}.}
\item{Strumenti per filtri bayesiani\footnote{ Oltre ai filtri di Kalman e alle loro varianti, la libreria offre strumenti generali per l'inferenza bayesiana e il filtraggio}.}
\item{Altre funzionalità accessorie\footnote{Vi sono poi funzionalità complementari che semplificano l'implementazione di sistemi dinamici come: l'ottimizzazione numerica, degli strumenti di simulazione (come dei generatori di rumore gaussiano e non) e vi è il supporto per modelli personalizzati di sistemi che si vogliono realizzare.}.}
\end{itemize}

\subsection{\lstinline|pykalman|~\cite{githubGitHubPykalmanpykalman}}

Tale libreria, a differenza di quella precedente è tutta basata sul filtro di Kalman, essa di fatto fornisce una serie di strumenti per implementare e lavorare con modelli di Kalman. Essa è essenzialmente suddivisa in due parti:
\begin{itemize}
\item{Filtro di Kalman canonico.}
\item{Unscented Kalman Filter.}
\end{itemize}

Come vedremo nel seguito, nessuna di queste categorie è utile per una realizzazione efficace che possa funzionare con segnali audio. Pertanto, anche trattandosi di una libreria potente e relativamente semplice da usare, la lasceremo da parte nella nostra trattazione.

\subsection{\lstinline|numpy|~\cite{numpyNumPy}}

Osservando le molte altre librerie reperibili in rete, si è deciso in ultima istanza di partire da una situazione più basilare, nella quale il filtro di Kalman esteso, sarebbe stato realizzato quasi da zero, servendosi di strumenti matematici già realizzati.

In soccorso a tali presupposti è stata selezionata, una delle librerie più utilizzate di Python: \lstinline|numpy|.

Essa è è una delle principali librerie Open Source di Python utilizzata per il calcolo numerico e scientifico è stata sviluppata per poter gestire array multidimensionali e operazioni matematiche avanzate, inoltre essa è stata progettata per offrire prestazioni elevate e flessibilità. Su di essa si basano anche altre note librerie scientifiche come SciPy, Pandas, TensorFlow e PyTorch (di cui non trattiamo ma che potranno essere note ai più).
\newpage
Tra le principali caratteristiche di tale libreria troviamo:
\begin{itemize}

\item{Supporto per la gestione e l'utilizzo degli array multidimensionali (\lstinline|ndarray|)\footnote{Utile per la realizzazione e gestione di matrici.}.}
\item{Funzionalità matematiche e statistiche, come un set di funzioni per operazioni aritmetiche, lineari e statistiche. }
\item{Gestione e generazione di dati casuali\footnote{Utile nel nostro contesto per la generazione e l'utilizzo di rumori gaussiani.}.}
\item{Supporto per l'algebra lineare.}
\item{Manipolazione e modifica di array.}
\item{Compatibilità e interoperabilità, infatti \lstinline|numpy| è compatibile con file di dati esterni, come file di testo e file binari e può anche interfacciarsi con librerie scritte in linguaggi di basso livello come C e Fortran.}
\end{itemize}

\subsection{Realizzazione del filtro di Kalman con la libreria \lstinline|numpy|}

A puro scopo esemplificativo all'interno dei due brevi paragrafi seguenti, vi saranno le indicazioni introduttive a delle realizzazioni del filtro di Kalman standard e della versione estesa realizzate in Python utilizzando la libreria \lstinline|numpy|.

Come oramai sarà chiaro al lettore, il listato del codice di cui si tratta è presente all'interno dell'Appendice e sarà di volta in volta riportata la sezione in cui è presente il listato.

\subsubsection{Realizzazione di un filtro di Kalman standard con Python e \lstinline|numpy|}\label{se:kalman-standard-numpy}

Per poter realizzare un filtro di Kalman canonico con \lstinline|numpy|, realizziamo una classe con tre funzioni:
\begin{itemize}
\item{Funzione per l'inizializzazione.}
\item{Funzione per la predizione.}
\item{Funzione per l'aggiornamento.}
\end{itemize}

Queste tre funzioni chiamate rispettivamente \lstinline|__init__|, \lstinline|predict| ed \lstinline|update| forniscono gli strumenti necessari alla realizzazione di un filtro di Kalman. La funzione di inizializzazione permette di inserire all'interno dell'algoritmo i parametri iniziali per poter far funzionare lo stimatore, essa è chiamata (come in Java) costruttore, successivamente possiamo procedere ad un'esecuzione del programma per mezzo di un ciclo \lstinline|for| che sfrutta le due funzioni di predizione prima e di aggiornamento poi, per fornire la stima del valore che stiamo cercando.

Un'esempio di output è il seguente:
\begin{lstlisting}
Predizione:[1 1], Stima aggiornata:[1. 1.]
Predizione:[2. 1.], Stima aggiornata:[2. 1.]
Predizione:[3. 1.], Stima aggiornata:[3. 1.]
\end{lstlisting}

Il codice Python per l'implementazione del filtro di Kalman standard reperibile in appendice nella sezione \ref{se:python-kalman-standard}.

\iffalse
%https://thekalmanfilter.com/extended-kalman-filter-python-example/
\fi

\subsubsection{Realizzazione di un filtro di Kalman esteso con Python e \lstinline|numpy|}\label{se:kalman-extended-numpy}
Come per l'esempio precedente, si fa uso (all'interno della classe che implementa il filtro di Kalman esteso) delle seguenti funzioni (adeguatamente modificate per l'occasione):
\begin{itemize}
\item{Funzione per l'inizializzazione.}
\item{Funzione per la predizione.}
\item{Funzione per l'aggiornamento.}
\end{itemize}

A tali funzioni della classe chiamata \lstinline|ExtendedKalmanFilter|, si aggiungono una funzione che descrive un sistema non lineare\footnote{Nel nostro caso il sistema descrive un moto circolare per il quale ricordiamo che in presenza di forze non lineari (ad esempio, attrito viscoso o forze di tipo elastico non lineare) ricade nella casistica dei sistemi non lineari.} per simulare un sistema al quale applicare il filtro esteso di Kalman, una funzione per l'osservazione del sistema non lineare, e le funzioni utili per la linearizzazione del sistema non lineare, che ricordiamo fare uso di matrici Jacobiane.

Un'esempio di output del seguente programma è:
\begin{lstlisting}
Passo 1
Predizione dello stato: [1.  0.1 1.  0.1]
Stima aggiornata dello stato: [1.  0.1 1.  0.1]

Passo 2
Predizione dello stato: [1.99500417 0.2        1.         0.1       ]
Stima aggiornata dello stato: [1.99879739 0.19993027 1.00215044 0.10001762]

Passo 3
Predizione dello stato: [2.98098542 0.29994789 1.00215044 0.10001762]
Stima aggiornata dello stato: [2.99471509 0.29945136 1.00876764 0.09991389]
\end{lstlisting}

Il codice Python per l'implementazione del filtro di Kalman esteso reperibile in appendice nella sezione \ref{se:python-kalman-extended}.

\subsection{Altre librerie utilizzate}

Oltre alla libreria \lstinline|numpy| si farà uso nel seguito della trattazione delle seguenti librerie, di cui riportiamo delle brevi introduzioni.

\subsubsection{\lstinline|matplotlib|~\cite{matplotlibMatplotlibx2014}}

Matplotlib è una libreria per la visualizzazione dei dati in Python utilizzata in contesti accademici e industriali per creare grafici e rappresentazioni grafiche di dati, essa supporta un'ampia varietà di tipi di grafici, tra cui quelli lineari, a dispersione, istogrammi e grafici a barre, adattandosi a molteplici esigenze analitiche.
La sua caratteristica principale è la capacità di fornire un controllo dettagliato sull'aspetto dei grafici, consentendo personalizzazioni avanzate per soddisfare specifici requisiti di presentazione.

\subsubsection{\lstinline|librosa|~\cite{librosaLibrosa}}

Essa è una libreria Python che offre funzioni per l'analisi e l'elaborazione del segnale audio, con un focus su aspetti musicali e sulla rappresentazione dei segnali sonori. Tra le sue capacità principali vi è la trasformazione del segnale audio in rappresentazioni diverse, come spettri dei segnali o rappresentazioni basate su scale percettive come la scala Mel. Questi strumenti sono fondamentali per studiare e analizzare la struttura musicale nell'elaborazione dei segnali audio e per estrarre informazioni rilevanti da essi.
Oltre alle trasformazioni di base, \lstinline|librosa| implementa metodi avanzati per l'estrazione dei descrittori audio, come ad esempio MFCC, Spectral Centroid, e Spectral Contrast, è inoltre in grado di elaborare l'analisi della tonalità del segnale sonoro che si sta osservando tramite grafici ed è in grado di studiare e rilevare le caratteristiche ritmiche. Essa è inoltre corredata da strumenti utili per la segmentazione del segnale sonoro (ad esempio attraverso l'identificazione di eventi significativi o cambiamenti di struttura all'interno di una traccia audio) permettendo quindi la manipolazione del segnale, come il time-stretching e il pitch shifting. Ha di base funzionalità per il caricamento e il salvataggio di file audio in diversi formati, oltre a contenere strumenti per il filtraggio e la sintesi. Da ultimo essa è progettata per interagire con strumenti matematici e grafici che consentono l'analisi e la visualizzazione grafica dei file audio.

\subsubsection{\lstinline|soundfile|~\cite{githubGitHubBastibepythonsoundfile}}
Essa è una libreria di Python progettata per la lettura e la scrittura di file audio in diversi formati, è basata su libsndfile~\cite{githubGitHubLibsndfilelibsndfile} \footnote{Una libreria C altamente efficiente e ampiamente utilizzata nel trattamento dell'audio digitale.}. Consente di lavorare con file WAV, FLAC, AIFF e molti altri formati senza modificare la qualità del suono e si distingue per la sua capacità di gestire sia file audio PCM non compressi che formati compressi, garantendo flessibilità nella gestione dei progetti audio. Permette inoltre di leggere e scrivere i dati audio come array interagendo con la libreria \lstinline|numpy|, facilitando l'integrazione con altre librerie scientifiche e strumenti di analisi dei dati. Ciò la rende particolarmente utile per applicazioni che spaziano dall'elaborazione del segnale al machine learning, fino alla sintesi del suono. Inoltre, la libreria offre funzionalità avanzate come la lettura e scrittura di metadati, la gestione di file audio multicanale e la possibilità di lavorare con campioni di diverse profondità di bit e frequenze di campionamento.

\subsubsection{\lstinline|os|~\cite{pythonMiscellaneousOperating}}

Essa è una delle componenti fondamentali del linguaggio Python, ed è stata realizzata per facilitare l'interazione con il sistema operativo e Python. Fornisce una serie di funzionalità che consentono di eseguire operazioni a livello di sistema, come la gestione dei file, l'interazione con l'ambiente di esecuzione, etc... Attraverso tale libreria, è possibile accedere alle variabili d'ambiente del sistema, eseguire comandi del sistema, navigare tra le directory, creare e rimuovere file e cartelle, oltre alla possibilità di ottenere informazioni sullo stato del sistema.

Essa va di pari passo con il concetto di portabilità informatica descritto precedentemente: permette infatti di scrivere codice che può essere eseguito su diverse piattaforme (come Windows, macOS e Linux) senza dover adattare il codice specificamente a ciascun sistema operativo, essa è una risorsa indispensabile per i programmatori, ed aiuta molto lo sviluppo di codice in contesti che richiedono un'interazione diretta con il sistema operativo.

\subsubsection{\lstinline|argparse|~\cite{pythonArgparseParser}}

Tale libreria permette di gestire ed interpretare i parametri forniti da riga di comando nell'atto di eseguire uno script. Essa contiene quindi un insieme di funzioni utili per realizzare il \textit{parsing} di tutto ciò che viene inserito come parametro a corredo di uno script Python, permettendo di interpretare e formattare adeguatamente il testo inserito. 

