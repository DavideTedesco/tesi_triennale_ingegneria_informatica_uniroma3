%!TEX TS-program = latex
%!TEX encoding = UTF-8 Unicode
% !TEX root =   Davide_Tedesco-Tesi.tex

\section{Proprietà dei segnali utilizzati e tipologie dei segnali utilizzati}%capire se rimuoverla

Durante la trattazione sino ad ora affrontata, non abbiamo mai espresso quali potessero essere le tipologie di segnali da dover restaurare tramite l'algoritmo presentato. Essi sono infatti di natura e tipologia diversa e potrebbero essere utilizzati in fasi di testing e di utilizzo completamente estranee le une alle altre.

\subsection{Segnali con rumore generato aggiunto}\label{se:rumore-generato}

La prima tipologia di segnale, utile in fase di test dell'algoritmo, sono segnali che in partenza sono privi di rumore, ai quali viene aggiunto volutamente un rumore gaussiano e dei click. Tale particolare segnale è utile per poter comprendere se il funzionamento dell'algoritmo è adeguato, poiché conoscendo il segnale di partenza, si può comprendere se la restaurazione dello stesso abbia introdotto aberrazioni precedentemente non presenti.
Qui viene riportato un esempio di un segnale al quale viene aggiunto del rumore. Vi è in alto il segnale originario e sotto quello con il rumore aggiunto. È facile notare come può essere simulata una diversa tipologia di rumore in base a quello che andiamo ad aggiungere al segnale originale.

\begin{figure}[hbt!]
\centering
\includegraphics[width=10cm]{figure/drumsMlp_noisy_plot.png}
\caption{Esempio di un segnale e dello stesso segnale a cui sono stati applicati un rumore gaussiano ed un rumore che prevede la presenza di click.}
\end{figure}\label{fig:drums+noise}

Tale tipologia di rumore è stata generata con parte del codice della funzione \lstinline|main| del nostro caso di studio, presente in appendice \ref{se:python-audio-restoration}, che riportiamo di seguito (solo per semplicità di comprensione):

\begin{lstlisting}[language=Python]
def main(audio_path):
    # Carica l'audio dal percorso specificato
    print(f"Caricamento audio da: {audio_path}")
    y, sr = librosa.load(audio_path)

    # Parametri del rumore
    sigma_e = 0.5
    sigma_w = 0.2
    sigma_z = 0.1

    # Generazione del segnale rumoroso
    T = len(y)
    e = np.random.normal(0, sigma_e, T)
    w = np.random.normal(0, sigma_w, T)
    z = np.random.normal(0, sigma_z, T)

    v = np.zeros(T)
    click_probability = 0.05
    for t in range(T):
        if np.random.rand() < click_probability:
            v[t] = np.random.choice([-1, 1]) * np.random.uniform(10, 50)
    y_noisy = y + z + v
    ...
\end{lstlisting}

In essa possiamo notare la generazione dei due rumori e la somma del segnale originario a tali rumori:
\begin{equation}
 y_{\text{noisy}} = y + z + v
\end{equation}

In cui \(z\) è un rumore gaussiano, mentre \(v\) è un rumore che presenta dei click (ovvero degli impulsi unitari).

\subsection{Segnali con rumore presente nella registrazione e con parti mancanti}

Altra tipologia di segnali sono invece quelli che presentano già di loro natura del rumore: come rumore di fondo o presenza di click.

In tali casi, può essere molto complesso restaurare il segnale, ne riportiamo degli esempi che mostrano dalla loro forma d'onda, sia dei click che un rumore di fondo costante.
 
\begin{figure}[hbt!]
\centering
\includegraphics[width=12cm]{figure/Thomas_A._Edison-Mary_had_a_little_lamb_original_plot.png}
\caption{Forma d'onda della prima registrazione realizzata da Thomas Alva Edison su un cilindro di cera tramite un fonografo, nel 1877.}
\end{figure}\label{fig:sono1}

Seppur molto simili, notiamo nel secondo di tali segnali, una presenza molto più consistente del rumore di fondo, riconoscendo come tale differenza possa essere utile per testare l'algoritmo con segnali che presentano diverse caratteristiche.

\begin{figure}[hbt!]
\centering
\includegraphics[width=12cm]{figure/Armstrong_Moon_original_plot.png}
\caption{Forma d'onda della registrazione dello storico atterraggio sulla luna, nella quale la voce di Neil Armstrong recita: \textit{That's one small step for a man, one giant leap for mankind}.}
\end{figure}\label{fig:sono2}

\section{Risultati numerici}

Alcuni risultati numerici riguardanti la bontà e il funzionamento dell'algoritmo qui presentato, sono riportati in ~\cite{Canazza2010}, nel quale vengono confrontate diverse tipologie di algoritmi per la restaurazione dei segnali sonori, sia commerciali che Open Source.

\begin{table}[h!]
\centering
\begin{tabular}{|l|c|c|c|}
\hline
\textbf{Tipologia di sistema}       & \textbf{Tom’s diner} & \textbf{Nofrio ritorna dall’America} & \textbf{Media complessiva} \\ \hline
EKF                               & +4.70                & +3.00                                & +3.850                 \\ \hline
CEDAR Tools                       & +5.00                & +3.85                                & +4.425                 \\ \hline
NoNOISE restoration suite         & +4.15                & +2.78                                & +3.465                 \\ \hline
Waves Restoration bundle          & +4.30                & +2.55                                & +3.425                 \\ \hline
Audacity                          & +4.15                & +2.25                                & +3.200                 \\ \hline
Cube-Tec restoration              & +4.12                & +0.69                                & +2.405                 \\ \hline
Adobe Audition                    & +3.90                & +0.55                                & +2.225                 \\ \hline
Anchor 7 kHz                      & -1.50                & +0.50                                & -0.500                 \\ \hline
Anchor 3.5 kHz                    & -5.00                & -4.80                                & -4.900                 \\ \hline
\end{tabular}
\caption{Tabella riportante le diverse tipologie di sistemi (ed algoritmi) utilizzati per la restaurazione del segnale.}
\label{tab:restoration}
\end{table}

Da tale confronto notiamo come il filtro di Kalman esteso sia il secondo algoritmo più convincente per la restaurazione dei segnali sonori.

Nel test descritto in ~\cite{Canazza2010} sono stati utilizzati due brani musicali di diversa tipologia:
\begin{itemize}
\item{\textit{Tom's Diner} (Solitude Standing, Suzanne Vega, 1987, A\&M, VEGCD3), per il quale è stato sintetizzato un rumore a banda larga e aggiunto al segnale audio, insieme a rumore impulsivo come descritto in \ref{se:rumore-generato}.
}
\item{\textit{Nofrio ritorna dall’America} di G. de Rosalia \& Co, registrato nel 1922 (New York) su un disco in vinile a 78 giri/min (Columbia 88447).}
\end{itemize}
%=====================

%\section{Forma d'onda dei segnali restaurati}

\iffalse%------


\begin{figure}[hbt!]
\centering
\includegraphics[width=10cm]{figure/kalman-filter--scilab-xcos_upscayl_4x_realesrgan-x4plus.png}
\caption{Forma d'onda.}
\end{figure}
\fi%------

%\section{Commenti alla restaurazione realizzata}