%!TEX TS-program = latex
%!TEX encoding = UTF-8 Unicode
% !TEX root =   Davide_Tedesco-Tesi.tex
\section{Filtraggio di un segnale audio}

Un filtro, nel contesto audio, è un mezzo attraverso il quale un segnale sonoro può transitare. In genere si pensa ad un filtro, come a qualcosa per cui il segnale passandovi attraverso, subisca una qualche modifica.  Ad esempio un cavo non è in genere considerato un filtro (anche se sotto alcune circostanze lo può divenire), mentre un'altoparlante lo è, poiché realizza una mutazione sul segnale nello stadio finale dello stesso che lo proietta in aria. Allo stesso modo le diverse vocali prodotte da un umano, sono a noi riconoscibili attraverso il cambiamento della forma della cavità orale, che fa cambiare di conseguenza le risonanze della stessa, filtrando e modificando le caratteristiche riprodotte dal tratto vocale.~\cite{stanfordWhatFilter} Possiamo pensare allo stesso modo a degli equalizzatori grafici, riverberi ed altri dispositivi presenti all'interno della nostra vita di tutti i giorni, come filtri audio. Ovviamente la modificazione realizzata da tali dispositivi è in genere voluta, ma in alcuni contesti come il filtraggio di un segnale e le risonanze all'interno di una stanza, tali mutazioni portano ad effetti sgradevoli (pensiamo ad esempio ad una stanza con pareti molto riflettenti acusticamente, che genera rigonfiamenti su alcune zone frequenziali).\newline
Nell'ambito del processamento dei segnali, si dice spesso:
\begin{displayquote}
When you think about it, everything is a filter.\footnote{Pensandoci, ogni cosa è un filtro.}
\end{displayquote}

Tale affermazione ci riporta quindi al primo capitolo, nel quale avevamo osservato un filtro come una scatola nera, in grado di elaborare in un qualche modo ciò che vi è inserito al suo interno, ottenendo in uscita da esso un altro segnale. Ovviamente la natura del segnale in uscita non sarà totalmente decorrelata da ciò che vi è posto in ingresso al filtro, ma sarà influenzata dall'ingresso.\newline
Sono necessarie ulteriori specifiche per andare a comprendere in maniera più chiara l'applicazione di un filtro ad un segnale audio, partendo sicuramente da una piccola introduzione ai segnali audio analogici e digitali, per poi osservare quali tipologie di filtri sono in genere utilizzati (astraendo dai contesti di analogico e digitale).


\subsection{Introduzione ai segnali audio}

\begin{figure}[hbt!]
\centering
\includegraphics[width=10cm]{figure/segnale_campionato_upscayl_4x_realesrgan-x4plus.png}\caption{Esempio di un segnale campionato e quantizzato partendo da un segnale analogico.}
\end{figure}

Un segnale audio, è in genere la rappresentazione elettrica attuata tramite una qualche tipologia di trasduttore che ha permesso di raffigurare e riportare uno spostamento di aria (il suono vive infatti nell'aria a noi circostante) all'interno di un dispositivo elettrico o elettronico. Nello specifico i segnali audio con cui è a noi possibile lavorare sono rappresentati in maniera analogica, attraverso supporti o mezzi tecnologici che prevedono l'utilizzo di sole componenti elettriche; oppure attraverso supporti e mezzi digitali, che quindi vedono all'interno della catena elettroacustica la comparsa di un convertitore\footnote{DAC ovvero Digital to Analog Converter per il passaggio dal regime digitale a quello analogico o ADC - Analog to Digital Converter, per il passaggio inverso.} in grado di portare il segnale da uno stadio analogico ad uno digitale.\newline
Ovviamente i segnali analogici non necessitano di alcuna conversione per poter essere manipolati ed elaborati in analogico e si può quindi affermare che essi viaggino alla velocità della luce (dato che di corrente elettrica stiamo parlando), senza costrizioni frequenziali di sorta. Per i segnali digitali, si necessita invece di una rappresentazione degli stessi in un dominio discreto che è stato storicamente basato e costruito in base alle caratteristiche del nostro apparato uditivo. Quest'ultimo è in grado di distinguere chiaramente segnali sonori tra i \(16 Hz\)  e i \(22000 Hz\)  circa ed è spesso tale banda frequenziale ad essere utilizza nella rappresentazione digitale del segnale. Bisogna però specificare perché spesso la frequenza di campionamento\footnote{Ovvero il numero di istanti temporali in cui viene suddiviso un singolo secondo di un segnale analogico dal DAC.} utilizzata è il doppio della frequenza massima che si vuole rappresentare, ovvero per il Teorema di Shannon~\cite{sciencedirectShannonSampling}. Tale teorema afferma che la minima frequenza di campionamento utilizzabile per poter campionare un segnale (non solamente sonoro) è pari al doppio della frequenza massima che si vuole rappresentare, chiamata \textit{frequenza di Nyquist}, per non incorrere in aberrazioni di varia natura e per poter preservare tutte le informazioni utili dal segnale di partenza\footnote{Anche se il doppio della frequenza utile del segnale che si vuole rappresentare è identificato come il minimo valore della frequenza di campionamento da dover utilizzare, è spesso consigliato utilizzare anche frequenze di campionamento più elevate per avere nelle fasi di processamento del segnale stesso maggiore banda disponibile e non incorrere in problemi di aliasing.}. È importante notare che non solo la rappresentazione del segnale nel tempo è importante, ma è rilevante anche l'ampiezza del segnale che deve essere rappresentato nel dominio digitale. Mentre per la rappresentazione del segnale nel tempo si applica il campionamento dello stesso, per la rappresentazione delle ampiezze si utilizza la quantizzazione. Queste due informazioni vengono quindi combinate a creare la rappresentazione del segnale audio in digitale ad una data frequenza di campionamento ed una specifica profondità misurata in bit (per la quantizzazione).

%Come si evince dalle Figure~\ref{fig:figura-doppia}.a e~\ref{fig:figura-doppia}.b non si capisce molto.

\subsection{Tipologie di filtri utilizzati nel dominio audio}\label{se:tipologie-filtri}
%da umanizzare, sembra troppo robotica la lista
Osservato quindi come è possibile rappresentare un segnale in analogico o in digitale, vediamo quali siano le tipologie di filtri in genere utilizzati applicate ai segnali audio.
Riportiamo di seguito la lista dei più comuni filtri:
\begin{itemize}
\item{Filtro passa-basso, è un tipo di filtro che permette il passaggio delle frequenze basse e attenua o blocca le frequenze alte. È in genere progettato in modo da lasciare passare solo le frequenze inferiori a alla frequenza di taglio, attenuando quelle superiori ad essa. Ad esempio, un filtro passa basso con frequenza di taglio a 1 kHz permetterà il passaggio delle frequenze sotto 1 kHz e ridurrà progressivamente l’intensità delle frequenze superiori.
}
\item{Filtro passa-alto, esso è un tipo di filtro che lascia passare le frequenze alte di un segnale e attenua (ovvero riduce l'intensità) quelle basse, esso è progettato per avere una frequenza di taglio specifica, ovvero un valore oltre il quale le frequenze passano e al di sotto del quale vengono ridotte. Se, ad esempio, la frequenza di taglio di un filtro passa alto è 1 kHz, tutte le frequenze al di sopra dei 1 kHz passeranno senza essere modificate, mentre quelle sotto verranno attenuate.
}
\item{Filtro passa-banda, esso è un tipo di filtro che permette il passaggio di segnali solo all'interno di una specifica banda di frequenze, attenuando quelle al di fuori di essa. In altre parole, un filtro passa banda consente alle frequenze comprese tra una frequenza minima e una frequenza massima di attraversarlo, mentre blocca o attenua le frequenze al di sopra e al di sotto di questo intervallo di frequenze. Ad esempio se immaginiamo di voler isolare una trasmissione radio di una stazione che trasmette sulla frequenza di 100 MHz, potremo utilizzare un filtro passa banda impostato tra 99,5 MHz e 100,5 MHz, bloccando tutto il resto.
}
\item{Filtro notch, esso è un filtro particolare progettato per attenuare segnali in una specifica e ristretta banda di frequenza, lasciando passare tutte le altre frequenze al di fuori di essa. Viene chiamato filtro \textit{notch} per via della forma caratteristica della risposta in frequenza, che mostra una "valle" nel punto in cui l'ampiezza del segnale è più attenuata.
}
\item{Filtro all-pass, esso è un tipo di filtro che lascia inalterata l'ampiezza del segnale che vi passa attraverso a tutte le frequenze, ma modifica la fase. Diversamente dai filtri passa-basso, passa-alto o passa-banda, che alterano sia ampiezza che fase in base alla frequenza, il filtro allp-ass agisce solo sulla fase del segnale. Questo significa che tutte le frequenze passano con la stessa ampiezza, ma vengono ritardate in maniera diversa a seconda della loro frequenza e di come è costruito il filtro, in pratica il filtro all-pass è utile perché permette di manipolare la fase del segnale senza alterare l'ampiezza, aprendo la strada a una gamma di applicazioni dove la coerenza della fase o la manipolazione di questa è cruciale.
}
\item{Filtro shelving, è un tipo di filtro che viene usato per modificare il livello dell'ampiezza di una certa banda di frequenze sopra o sotto un certo punto. Praticamente è come una sorta di "gradino" da cui prende il nome dall'inglese \textit{shelving}. Questo tipo di filtro può essere usato sia per aumentare che per diminuire il volume di certe frequenze, al di sopra o al di sotto della frequenza di taglio.
}
\item{Filtro risonante, esso è un particolare filtro in grado di entrare in risonanza alla frequenza specifica indicata dal filtro, ovvero permette di amplificare il segnale a tale frequenza, invece di attenuarlo}
\item{Altre tipologie di filtri, vi sono poi altre tipologie di filtri qui non riportate che implementano in genere combinazioni dei filtri qui visti o topologie totalmente diverse ed innovative che introducono concetti di filtraggio avanzati che si discostano dall'interesse di questo testo.}
\end{itemize}

A puro scopo esemplificativo, riportiamo di seguito l'applicazione di un filtro passa basso ad un segnale contenente un rumore bianco\footnote{I listati utilizzati per la generazione delle immagini e degli esempi realizzati in Python, sono presenti all'interno dell'Appendice \ref{se:appendice}.
}:
\begin{figure}[hbt!]
\centering
\includegraphics[width=10cm]{figure/rumore_bianco_filtrato_5000_confronto.png}\caption{Esempio riportante l'utilizzo di un filtro passa basso a 5000 Hz su di un rumore bianco.}\label{fig:filtro-passa-basso}
\end{figure}
%capire se inserire altro
\section{Tipologie di realizzazioni pratiche dei filtri}

Nell'ambito audio vi sono, come anche gli altri ambiti di elaborazione e processamento del segnale, diversi metodi di realizzazione dei filtri. Nell'ultimo secolo infatti vi è stato un radicale passaggio dal mondo analogico al mondo digitale che ha comportato l'apertura a nuovi paradigmi e nuove possibilità riguardanti il filtraggio, ma ha anche costretto generazioni di ingegneri, tecnici e musicisti a realizzare il \textit{porting} di quei filtri comunemente utilizzati nell'industria musicale verso il digitale. Si può quindi affermare che il mondo del filtraggio digitale, si è via via andato ad utilizzare sempre di più costruendo dapprima le emulazioni dei filtri analogici e da ultime realizzando delle creazioni \textit{ex novo} di filtri digitali, realizzabili grazie alle nuove ed uniche possibilità del digitale.

\subsection{Filtraggio analogico}
Le tipiche configurazioni analogiche per la realizzazione del filtraggio sono in genere:
\begin{itemize}
\item{Filtri passivi\footnote{Ognuno di essi riporta all'interno della propria dicitura il/le componente/i specifiche di realizzazione dello stesso.
Ricordiamo che con la \textit{R} si identifica un resistore, con la \textit{C} un condensatore e con la \textit{L} un induttore.
}}
\begin{itemize}
\item{Filtro RC, realizzato con resistori e condensatori, è in genere utilizzato per la realizzazione di filtri passa-basso e passa-alto.}
\item{Filtro LC, realizzato con induttori ed condensatori, è in genere utilizzato per la realizzazione di filtri passa-banda e notch dato che l'induttore permette una maggiore selettività nel filtraggio.}
\item{Filtro RLC, si avvale di tutte le tipologie di componenti elettroniche utilizzate sino ad ora, questa configurazione consente di realizzare con una precisione maggiore nel controllo delle frequenze, i filtri visti sino ad ora. È per tali motivi che è spesso utilizzato per contesti in cui è necessario un dettaglio ed una precisione maggiore nel filtraggio.}
\item{Altre tipologie di filtri passivi, ovvero che non fanno uso di alimentazione per effettuare l'operazione di filtraggio.}
\end{itemize}
\item{Filtri attivi\footnote{A differenza dei filtri osservati precedentemente essi necessitano di un'alimentazione propria per poter funzionare.}:}
\begin{itemize}
\item{Filtro realizzato con amplificatore operazionale, si tratta in genere di filtri realizzati con amplificatori operazionali ed altre componenti analogiche (come resistori e condensatori) che permette di realizzare tutte le tipologie di filtri viste sino ad ora.}
\item{Filtri a transistor, utilizzati spesso al posto degli amplificatori operazionali, per eseguire filtraggio alle alte frequenze.}
\item{Altre tipologie di filtri attivi, ovvero che fanno uso di alimentazione per effettuare l'operazione di filtraggio.}
\end{itemize}
\end{itemize}


\subsection{Filtraggio digitale}

Un filtro digitale è un filtro che opera su segnali digitali, quindi osservando e processando segnali rappresentati all'interno di un di un dominio digitale. Il funzionamento si può schematizzare in questa maniera: esso prende una sequenza di numeri (il segnale in ingresso campionato) e produce una nuova sequenza di numeri (il segnale di uscita filtrato). È importante comprendere che un filtro digitale può fare tutto ciò che un filtro nel mondo reale (analogico) può fare. Quindi, tutti i filtri sino ad ora osservati possono essere simulati digitalmente con un grado arbitrario di precisione. Pertanto, un filtro digitale è solo una formula per passare da un segnale digitale a un altro. Può esistere come un'equazione su carta, come una subroutine di un computer o come un insieme di circuiti integrati.~\cite{Smith2007-gg}\newline
Nel dominio digitale, come visto precedentemente, bisogna discretizzare i valori continui tramite l'utilizzo del Teorema del campionamento e quindi campionando e quantizzando i segnali da elaborare.
%inserire spiegazione trasformata di Laplace e trasformata Z
Andando a lavorare direttamente su segnali campionati, non sarà più utile lavorare con uno strumento come la Trasformata di Laplace\footnote{Strumento matematico per risolvere equazioni differenziali che permette di passare dal dominio del tempo al dominio complesso di Laplace, semplificando il calcolo e la manipolazione delle equazioni differenziali, permettendo di studiarle come semplici equazioni algebriche.} ma si potrà utilizzare la Trasformata Z, che è di fatto l'omologo della trasformata di Laplace per segnali discreti.
\begin{equation}
\mathscr{L}_{-}\{f(t)\} = F(S) 
\end{equation}

\begin{equation}
t \rightarrow s 
\end{equation}

\begin{equation}
 \int_{0^-}^{+\infty}f(t) e^{-st} dt
\end{equation}
%https://www.notion.so/Fondamenti-di-Automatica-8eb190133f0e48c1a7de6bdcf03d02ea?pvs=4#a9bd5791377843f28b3d6c5d999c613c
%https://www.notion.so/Fondamenti-di-Telecomunicazioni-9242420d15ea4d93bf6336407335b121?pvs=4#fa426d523a244559a528cdf8e8e131f7

Trattando di Trasformata di Laplace, non si può fare altro che osservare le equazioni differenziali (continue nel tempo), tramite le equazioni alla differenze finite che ci permettono di passare da una generica \(y(t)\) con la variabile del tempo continua, ad una funzione del tipo \(y_k\) in cui la variabile \(k\) è discreta ed indica l'istante di campionamento a cui ci si riferisce. 

Osserviamone brevemente un esempio:
\begin{equation}
y_{_{k+1}} = 3y_{_k}+u_{_k}
\end{equation}

\begin{equation}
\text{con }u_{_k} = \sum_{k=0}^{\infty}1 
\end{equation}

\begin{equation}
y_{_0}=2
\end{equation}
Conosciamo dell'esempio osservato all'istante di campionamento \(k+1\) la sequenza in ingresso \(u_k\) che è costante e la sua condizione iniziale. La prima delle equazioni è di fatto un'equazione alle differenze del primo grado e come le equazioni differenziali ha delle condizioni iniziali. A differenza delle equazioni differenziali, il grado delle equazioni alle differenze è descritto dalla \(n\) riportata al pedice della somma di \(y_{_{k+n}}\). Date le condizioni iniziali e l'equazione alle differenze, potremo calcolarne i valori, ottenendo quindi i valori di uscita.\newline
Capiamo dunque che quello qui osservato, ci ha permetta di trattare le equazioni alle differenze nel discreto come un'equazione differenziale è nel continuo. Osserviamo quindi che abbiamo scritto una funzione di trasferimento e la abbiamo direttamente applicata ad un ipotetico segnale in ingresso. Ciò ci fa tornare in mente l'approccio della scatola nera osservato nel primo capitolo nella figura \ref{fig:black-box}, nella quale era stata descritto il funzionamento di un filtro. Essa è quindi l'applicazione basilare di un filtro digitale ad un segnale campionato.
\begin{figure}[hbt!]
\begin{center}
    \vspace{0.5cm} % Spazio sopra il diagramma
\begin{tikzpicture}

    % Definizione dei nodi
    \node[left] (input) at (0,0) {$x_k$}; % Nodo per l'ingresso
    \node[draw, text=black, minimum width=2cm, minimum height=1cm] (box) at (2,0) {Filtro digitale}; % Black Box centrale
    \node[right] (output) at (4,0) {$y_k$}; % Nodo per l'uscita

    % Disegno delle frecce
    \draw[->] (input) -- (box); % Freccia dall'ingresso alla Black Box
    \draw[->] (box) -- (output); % Freccia dalla Black Box all'uscita
\end{tikzpicture}
\vspace{0.5cm} % Spazio sopra il diagramma
\caption{Rappresentazione schematica di un filtro digitale con ingresso \( x_k \) ed uscita \( y_k \).}
\end{center}
\end{figure}
Non discuteremo ulteriormente del legame tra il dominio \(s\) e \(z\) per mancanza di coerenza con la trattazione successiva, ma basta comprendere che il legame tra questi due domini è forte e di mezzo (nel passaggio dal primo al secondo) vi è una discretizzazione delle informazioni attuata sempre tenendo a mente il Teorema del campionamento.\newline
%inserire spiegazione delay utilizzati per realizzare i filtri
Inseriamo solamente alcuni dettagli maggiori sulla realizzazione dei filtri digitali, per comprendere i passaggi successivi della trattazione. Per la realizzazione di filtri digitali si utilizzano delle linee di ritardo espresse in genere come \(z^{-n}\) in cui \(n\) identifica il numero di campioni di cui deve essere ritardato il campione in esame (ad esempio un ritardo di un campione è \(z^{-1}\)). Ciò significa che entrato il campione all'interno del blocco di filtraggio esso viene trattenuto per un ulteriore campione ed utilizzato all'istante successivo.
Tale operazione permette di eseguire operazioni in \textit{feedback}\footnote{Dall'inglese \textit{retroazione}.} ed in \textit{feedforward}\footnote{Dall'inglese \textit{propagazione in avanti}.} andando quindi a sfruttare campioni del segnale in ingresso per effettuare il filtraggio dello stesso.
%Altre tipologie di filtri oltre a quelli realizzabili come porting di quelli analogici, dai filtri IIR a quelli FIR...
Comprendiamo quindi che oltre alle categorie dei filtri realizzabili nel dominio analogico, nel dominio digitale sia necessaria un'ulteriore suddivisione degli stessi filtri:
\begin{itemize}
\item{Filtri IIR - Infinite Impulse Response, tale categoria di filtri emula il funzionamento dei filtri analogici utilizzando sia l'ingresso che i campioni passati ritardati per mezzo della retroazione. Hanno risposta infinita all'impulso, poiché come per i filtri analogici la risposta ad un impulso unitario inserito all'interno di tale filtro non ha una fine predeterminata.~\cite{sciencedirectIIRFilterOverview} Tali filtri vengono anche definiti filtri ricorsivi\footnote{Con il termine ricorsione, si intende una tecnica di programmazione in cui una funzione, durante la sua esecuzione, chiama se stessa per la risoluzione del problema proposto. Tale approccio permette di ridurre un problema complesso in piccoli sottoproblemi di natura meno complessa di quello originario.} (per differenziarli da quelli FIR che non sono ricorsivi).

\begin{figure}[H]
\begin{center}
    \vspace{0.5cm} % Spazio sopra il diagramma
\begin{tikzpicture}[auto, node distance=2cm]

    % Definizione degli stili dei nodi
    \tikzstyle{input} = [coordinate, label=above:$x(n)$]
    \tikzstyle{sum} = [draw, circle, minimum size=1cm, node distance=1cm, inner sep=0pt]
    \tikzstyle{block} = [draw, rectangle, minimum height=1cm, minimum width=1.5cm]
    \tikzstyle{output} = [coordinate, label=above:$y(n)$]
    
    % Nodi
    \node [input, name=input] at (0, 0) {};
    \node [sum, right of=input, node distance=2cm] (sum) {$+$};
    \node [output, name=output] at (7,0) {};
    \node [block] at (4,-2) (delay) {$z^{-1}$};

    % Connessioni
    \draw [->] (input) -- node {} (sum);
    \draw [->] (sum) -- node {} (output);
    \draw [-] (6,0) -- (6,-2);
    \draw [->] (6,-2) -- (delay);
    \draw [->] (delay) -| node[pos=0.25] {} (sum);

\end{tikzpicture}
\vspace{0.5cm} % Spazio sopra il diagramma
\caption{Rappresentazione schematica di un filtro IIR.}
\end{center}
\end{figure}
}
\item{Filtri FIR - Finite Impulse Response, al contrario dei filtri IIR, questa tipologia di filtri è realizzabile solamente nel dominio digitale, ciò garantisce a questa tipologia di filtri una stabilità molto maggiore rispetto ai filtri IIR e conseguentemente una fase lineare. Da contro, dato che tali filtri non hanno al loro interno retroazioni richiedono un maggior tempo di esecuzione per entrare in funzione (in genere nel campo audio poco percepibile).~\cite{sciencedirectFIRFilterOverview}

\begin{figure}[H]
\begin{center}
    \vspace{0.5cm} % Spazio sopra il diagramma
\begin{tikzpicture}[auto, node distance=2cm]

    % Definizione degli stili dei nodi
    \tikzstyle{input} = [coordinate, label=above:$x(n)$]
    \tikzstyle{sum} = [draw, circle, minimum size=1cm, inner sep=0pt]
    \tikzstyle{block} = [draw, rectangle, minimum height=1cm, minimum width=1.5cm]
    \tikzstyle{output} = [coordinate, label=above:$y(n)$]
    
    % Nodi
    \node [input, name=input] at (0, 0) {};
    \node [sum, right of=input] at (3,0) (sum) {$+$};
    \node [output, name=output] at (6,0) {};
    \node [block] at (3,-2) (delay) {$z^{-1}$};

    % Connessioni
    \draw [->] (input) -- node {} (sum);
    \draw [->] (sum) -- node {} (output);
    \draw [-] (1,0) -- (1,-2);
    \draw [->] (1,-2) -- (delay);
    \draw [->] (delay) -| node[pos=0.25] {} (sum);

\end{tikzpicture}
\vspace{0.5cm} % Spazio sopra il diagramma
\caption{Rappresentazione schematica di un filtro FIR.}
\end{center}
\end{figure}
}
\end{itemize}

\section{In che modo è legato il filtro di Kalman ai filtri qui osservati?}

Il filtro di Kalman, abbiamo visto essere uno stimatore ottimo nel paragrafo \ref{se:stimatore-ottimo}. È quindi naturale chiedersi, cosa abbia in comune esso con le tipologie di filtri riportate in questo capitolo.  I filtri osservati nella sezione \ref{se:tipologie-filtri} sono osservati da una prospettiva che osserva il dominio delle frequenze, ma dato che la nostra trattazione sul filtro di Kalman si è basata sino ad ora sul dominio del tempo, è corretto paragonare i primi al filtro di Kalman o ad una delle sue estensioni o generalizzazioni? 
\newline
Per poter passare dal dominio del tempo al dominio delle frequenze è necessario utilizzare una Trasformata di Fourier, che è in grado di suddividere un qualsivoglia segnale posto in ingresso ad essa nelle componenti sinusoidali che la compongono. Riflettendo proprio sulle operazioni compiute dal filtro di Kalman, potremo in un primo istante, pensare che tale filtro realizzi solamente uno \textit{smoothing} degli elementi che vengono posti in ingresso ad esso, e ciò porterebbe a pensare che tale filtro è un semplice filtro passa basso (utilizzato in genere proprio per questo compito). Ciò è in realtà parzialmente corretto, poiché il filtro di Kalman solamente in alcune situazioni effettua solamente un "addolcimento" dei segnali posti in ingresso ad esso. Attraverso la sua specifica realizzazione basata su predizione ed aggiornamento, consente di ottenere una varianza stabile che permette al guadagno di Kalman di raggiungere uno stato stabile. Solo dopo tale istante di funzionamento, in cui il guadagno calcolato diviene stabile si può pensare ad esso come ad un filtraggio lineare a coefficienti costanti. Ed utilizzare quindi i soliti strumenti che ci permettono di analizzare (in frequenza) il funzionamento di tale filtro.~\cite{substackKalmanFilter} Ovviamente avendo al suo interno una retroazione, lo potremo pensare come un filtro della tipologia IIR.