%!TEX TS-program = latex
%!TEX encoding = UTF-8 Unicode
% !TEX root =   Davide_Tedesco-Tesi.tex
\section{Rudolf Emil Kálmán, dall'Ungheria alla Luna}

Kálmán\footnote{Il quale da ora in poi sarà riportato come Kalman (privandolo degli accenti).}, nato a Budapest il 19 maggio del 1930, emigrò negli Stati Uniti all'età di 13 anni. In tale periodo il vento bellico e la pressione da parte della Germania costrinse la famiglia Kalman a trasferirsi. In tali circostanze egli inizia i suoi studi universitari presso il Massachusetts Institute of Technology (MIT) laurendosi nel 1953 in Ingegneria Elettrica completandoli l'anno seguente. Ottiene quindi il Dottorato di Ricerca nel 1957 presso la Columbia University iniziando ad interessarsi alle rappresentazioni di sistemi tramite variabili di stato\footnote{Rappresentazione in spazio di fase, ovvero la descrizione di un sistema dinamico in cui ci si riferisce alle variabili di stato del sistema in esame, formando con le stesse uno spazio vettoriale che rappresenta il sistema.}.
\newline
In tali anni approfondisce inoltre i suoi studi riguardanti la matematica applicata, sviluppando competenze e conoscenze utili che gli permisero di donare contributi molto rilevanti alla teoria dei sistemi di controllo.
Subito dopo il dottorato divenne ricercatore presso l'IBM Research Laboratory di Poughkeepsie, nello stato di New York; dove la sua ricerca si concentò sulla progettazione di sistemi di controllo lineare a dati campionati utilizzando criteri di prestazione quadratici, inoltre inizia ad interessarsi alla teoria di Lyapunov per l'analisi e la progettazione dei sistemi di controllo. In tale periodo inizia a comprendere la rilevanza che i computer avrebbero riposto nello studio ed utilizzo di sistemi a larga scala.\newline
Nel 1958 approda presso il Research Institute for Advanced Study (RIAS) di Baltimora dove rimarrà sino al 1964. È proprio in tale periodo che inizierà a fornire alla comunità scientifica il suo maggior contributo, fornendo utili studi ed applicazioni alla moderna teoria dei controlli.
Definisce in tali circostanze le basi teoriche per la definizione di \textit{Controllabilità} ed \textit{Osservabilità}\footnote{Concetti molto studiati nella teoria dei controlli che osserveremo nel seguito.}, collegando in maniera molto creativa ed originale aspetti matematici, teorici e pratici verso una teoria del controllo ottimo.\newline
Durante questo intenso periodo di studio, Kalman, pone le basi per il \textit{Filtro di Kalman}~\cite{Kalman1960} ottenendo i primi promettenti risultati già alla fine del 1958 ed all'inizio del 1959. Mettendo insieme i lavori di Wiener~\cite{Wiener2019}, Bode e Shannon~\cite{Bode1950} e molti altri, riguardanti la moderna teoria dello spazio di stato.\newline
Tra il 1960 ed il 1961 la soluzione al problema affrontato nel campo discreto gli fornisce, assieme al lavoro di Bucy~\cite{Kalman1961}, la risposta alla versione nel tempo continuo del \textit{Filtro di Kalman}.
Tale filtro, permetterà a Kalman, grazie alle sue estensioni (che osserveremo essere utili a risolvere problemi di natura non lineare e di altra tipologia) di poterlo applicare a quasi ogni applicazione che utilizzi la moderna teoria dei controlli.~\cite{ethwRudolfKalman}\newline
È proprio in un contesto in cui la NASA si apprestava a preparare i moduli lunari che tramite la missione Apollo 11 avrebbero portato l'uomo sulla luna, che il filtro di Kalman risulterà non solamente utile, ma decisivo per permettere agli astronauti di atterrare in sicurezza sul suolo lunare~\cite{nasaDiscoveryKalman,nasaReconstructionApollo}.

\begin{figure}[h]
\centering
\includegraphics[width=12cm]{figure/NASA_Kalman_filter.png}\caption{Note dal documento della NASA~\cite{nasaDiscoveryKalman} riportanti la scoperta della possibilità di utilizzo del filtro di Kalman come algoritmo utile per il calcolo della traiettoria del LEM (Lunar Excursion Module) verso il suolo lunare.}
\end{figure}

Ma l'applicazione degli elementi teorici definiti da Kalman venne realizzata nel corso degli anni anche a sistemi radar, al controllo dei processi o allo studio dei sistemi socioeconomici. È quindi importante sottolineare, come la popolarità dell'applicazione delle sue teorie fu dovuta al fatto che esse si prestavano ad essere implementate su di un calcolatore, esso veniva e viene utilizzato infatti sia per la fase di progettazione degli algoritmi, che per la fase di implementazione ed utilizzo degli stessi. Unendo quindi concetti del filtraggio e della teoria dei controlli in maniera intelligente ed inedita.\newline
Kalman iniziò dagli anni sessanta in poi, ad insegnare presso varie università americane ed europee~\cite{ethwRudolfKalman}
 che lo portarono a sviluppare una forte comprensione di come applicare proficuamente la teoria dei sistemi, adoperando e trovando approcci inerenti lo spazio di fase\footnote{Sottocategoria degli spazi di stato che rappresenta l'insieme di tutte le possibili configurazioni che un sistema fisico può assumere, in genere nello spazio delle fasi l'evoluzione di un sistema dinamico discreto è definita come una successione di punti, mentre quella di un sistema dinamico continuo è rappresentata da una curva.} e introducendo le nozioni di \textit{controllabilità}\footnote{Con il termine \textit{controllabilità} si identifica in un sistema dinamico, la sua capacità di poter raggiungere ogni punto dello spazio delle fasi.} ed \textit{osservabilità}\footnote{Avendo sempre a mente un sistema dinamico, con il termine \textit{osservabilità} si individua un sistema per il quale si può risalire allo stato del sistema partendo dalla conoscenza delle sue uscite.} oltre a definire la \textit{decomposizione strutturale di Kalman}\footnote{Applicandola si attua una decomposizione del sistema in esame in sottosistemi con specifiche caratteristiche di controllabilità, osservabilità e stabilità, essa è fondamentale per capire come un sistema può essere controllato e osservato. È molto utile nei sistemi complessi, dove è spesso necessario determinare quali parti del sistema sono effettivamente influenzabili o misurabili per ottimizzare il controllo delle stesse.}.\newline
I suoi sforzi vennero negli anni espletati tramite l'attribuzione di molti riconoscimenti di importanza scientifica planetaria, tra cui si ricordano la medaglia d'onore della IEEE\footnote{Institute of Electrical and Electronics Engineers}~\cite{ieeeRudolfKalman}, il \textit{premio Steele} della American Mathematical Society e nel 2008 la \textit{National Medal of Science} degli Stati Uniti d'America consegnata a Kalman dall'allora Presidente Barack Obama~\cite{nationalmedalsRudolfKlmn}.
Kalman, proseguendo instancabilmente nel suo impegno accademico e scientifico attraverso l'insegnamento, la pubblicazione di articoli, libri e ricerche, si spense nel mese di luglio del 2016, all'età di 86 anni. La sua scomparsa segnò la conclusione di una lunga carriera, durante la quale contribuì in modo significativo allo sviluppo di conoscenze fondamentali nel campo dell'ingegneria, della matematica e della ricerca scientifica. Kalman lasciò in eredità alle future generazioni di ingegneri, matematici e studiosi un patrimonio inestimabile di materiali, teorie e strumenti intellettuali, preziosi per costruire il progresso scientifico e tecnologico del futuro.


\section{Filtro: dall'etimologia all'utilizzo ingegneristico}

Il termine filtro, deriva dal tardo latino \textit{filtrum}~\cite{Pianigiani2022-ff} e dalla stessa etimologia di \textit{Feltro}. Esso, si riferiva inizialmente ad un panno, poi elemento in materiale di carta, che identificava un corpo poroso attraverso il quale si poteva far passare un liquido per ottenerne una parte più sottile o pura dello stesso.\newline
Da tale etimologia arriviamo ai nostri giorni, nei quali il significato di tale parola diviene più esteso e descrive non solamente il filtraggio di una sostanza attraverso un'altro materiale, ma si lega anche a concetti astratti e concreti, carichi di significati e riferimenti in quasi ogni campo di studio.

\begin{figure}[hbt!]
\centering
\includegraphics[width=8cm]{figure/hippocratic-sleeve-crop_upscayl_4x_realesrgan-x4plus.png}\caption{Disegno d'epoca riportante un sistema di filtraggio di un liquido realizzato con tessuti che si pensa sia stato ideato da Ippocrate.}
\end{figure}
Nel corso del ventesimo secolo, si andarono sviluppando sempre di più implementazioni di filtri in ambito elettronico ed elettrotecnico, utilizzati per svariati motivi, a partire ad esempio dalle applicazioni realizzate per gli inediti apparati telefonici.
Intorno agli anni sessanta del Novecento, i calcolatori iniziarono a diffondersi negli ambienti lavorativi più disparati, portando fisici, matematici, ingegneri e programmatori a sviluppare una versione dei filtri realizzata in digitale.\newline
Il progresso introdotto da tale novità portò non solo alla realizzazione dei filtri analogici in un dominio discreto\footnote{Per il quale è in genere utilizzato il termine \textit{porting}.} (come quello digitale), ma portò anche allo sviluppo di nuove tipologie di filtri, realizzabili unicamente all'interno del dominio discreto. 
%capire se inserire qualcos'altro
È importante notare quindi, che in termini ingegneristici un filtro è rappresentabile come una scatola nera (in inglese \textit{black box}), al cui interno vi entra un qualsivoglia elemento, rappresentabile ad esempio da un segnale $x(t)$ e che in uscita restituisce un altro segnale $y(t)$:
\begin{figure}[hbt!]
\begin{center}
    \vspace{0.5cm} % Spazio sopra il diagramma
\begin{tikzpicture}

    % Definizione dei nodi
    \node[left] (input) at (0,0) {$x(t)$}; % Nodo per l'ingresso
    \node[draw, fill=black, text=white, minimum width=2cm, minimum height=1cm] (box) at (2,0) {Black Box}; % Black Box centrale
    \node[right] (output) at (4,0) {$y(t)$}; % Nodo per l'uscita

    % Disegno delle frecce
    \draw[->] (input) -- (box); % Freccia dall'ingresso alla Black Box
    \draw[->] (box) -- (output); % Freccia dalla Black Box all'uscita
\end{tikzpicture}
\vspace{0.5cm} % Spazio sopra il diagramma
\caption{Rappresentazione schematica di un filtro con ingresso \( x(t) \) ed uscita \( y(t) \) realizzato con una \textit{Black Box}.}
\end{center}
\end{figure}\label{fig:black-box}
\newpage

Con tale rappresentazione si identifica quindi un qualsiasi oggetto in grado di realizzare un'elaborazione di ciò che viene posto in ingresso. Lo rimarchiamo in questa sezione, poiché il termine filtro risulterà centrale nelle pagine successive.
%aggiungere qualcosa
\section{Il filtro di Kalman: stimatore ottimo}\label{se:stimatore-ottimo}
%perché il filtro di Kalman è uno stimatore?
Il filtro di Kalman, data la definizione di filtro appena osservata, è l'applicazione numerica di un oggetto astratto in grado di realizzare una qualsivoglia elaborazione delle sequenze o dei segnali che vi vengono posti al suo interno. Essendo maggiormente precisi, il filtro di Kalman è definibile come uno \textit{stimatore}, inoltre possiamo affermare che tale tipologia di stimatore, realizza un algoritmo che tenta di realizzare una stima ottima del segnale o della sequenza che si sta osservando.
In particolare esso è utilizzato in genere in due casi specifici:
\begin{itemize}
\item{Per realizzare la stima dello stato di un sistema, quando esso non può essere stimato direttamente.}
\item{Quando è necessario stimare lo stato di un sistema soggetto a rumori.}
\end{itemize}

Nel primo caso descritto si tenta di trarre più informazioni possibili da ciò che è misurabile all'interno del sistema che si sta osservando, cercando quindi di ottenere delle misure che possano poi aiutarci a ricostruire una stima delle misure a noi necessarie.\newline
Nel secondo caso invece, è necessario (in genere) fondere le stime realizzate adoperando più sensori\footnote{Applicando la pratica descritta spesso come \textit{sensor fusion~\cite{sciencedirectSensorFusion}}.}, fondendo quindi le stime ottenute da tali sensori.\newline

L'applicazione di base realizzata da Kalman\footnote{Che verrà definita in seguito \textit{Canonical Kalman Filter}.} permette di stimare lo stato di un sistema dinamico lineare che presenti errori di misura o errori dovuti a rumore~\cite{Kalman1960}.
L'approccio introdotto da Kalman nel suo articolo del 1960 permette di stimare lo stato di un sistema dinamico, ovvero quell'insieme di variabili che descrivono in maniera dettagliata il comportamento di un sistema dinamico, tramite l'osservazione di misure contaminate da rumore. Ciò permette quindi di prevedere il comportamento futuro del sistema che si sta trattando.\newline

Kalman realizzò matematicamente un approccio ricorsivo in grado di aggiornare la stima dello stato ad ogni istante temporale, tramite l'osservazione delle misurazioni realizzate. Iniziando ad entrare nel particolare, si osserva che tale approccio utilizza in linea generale due fasi per ottenere la stima ottima del sistema:
\begin{itemize}
\item{Fase di Predizione, in cui il sistema è descritto tramite delle equazioni che ne modellano il funzionamento (la dinamica) permettendo di calcolare una previsione dello stato in base alla stima realizzata all'istante precedente osservato.}
\item{Fase di Correzione o Aggiornamento, che dopo aver ricevuto una nuova misura dal sistema, aggiorna la stima del sistema combinandola con la previsione integrando i dati della misura, migliorando notevolmente la precisione dello stimatore stesso.}
\end{itemize}
Tali fasi permettono, se realizzate in maniera ciclica, di minimizzare le incertezze del modello (e quindi l'errore del processo) e di diminuire drasticamente gli errori di misura (ovvero quelli derivati dalle misurazioni ottenute sul sistema stesso).\newline
A livello matematico possiamo definire due equazioni fondamentali:
\begin{itemize}
\item{L'equazione di stato, che descrive la dinamica del sistema: \begin{equation}x_k=Ax_{k-1}+Bu_k+w_k\end{equation}\newline Nella quale \(A\) è la matrice di transizione di stato\footnote{Essa descrive come lo stato del sistema si evolve da un'istante di tempo \(k-1\) a un istante \(k\) in assenza di controllo e misurazione, è utile per modellare la dinamica interna del sistema poiché definisce come le variabili di stato attuali influenzano quelle future.}, \(B\) è la matrice di controllo\footnote{Essa collega gli input di controllo \(u_k\) al sistema in esame, ovvero permette di comprendere come l'input esterno \(u_k\) influenzi il vettore di stato \(x_k\). In pratica tale variabile descrive l'azione delle variabili esterne al sistema.} ed infine \(w_k\) è il rumore del processo che viene assunto in genere come gaussiano con media nulla e varianza nota.
}
\item{L'equazione di misura, che riporta quindi le osservazioni realizzate sul sistema: \begin{equation}z_k=H_kx_k+v_k\end{equation} Nella quale \(H_k\) è la matrice di osservazione del sistema\footnote{Permette di descrivere come lo stato interno del sistema \(x_k\) si relaziona con le misure osservabili \(z_k\), quindi in breve essa collega le variabili dello stato del sistema con le misure che si possono effettivamente raccogliere. Ovvero consente di mappare le variabili di stato del sistema con i dati osservabili dallo stesso, ciò è importante perché spesso non è possibile osservare direttamente ogni variabile dello stato del sistema, e dunque \(H\)definisce quali aspetti dello stato possiamo realmente misurare e come possiamo misurarli.} e \(v_k\) è il rumore di misura che viene supposto anch'esso gaussiano con media nulla e varianza nota.
}
\end{itemize}
Definite le equazioni principali che permettono di realizzare il filtro di Kalman, comprendiamo che l'obiettivo finale di tale filtro è quindi stimare \(x_k\) tramite l'utilizzo delle misure realizzate sul sistema e le informazioni modellate osservando la dinamica del sistema in esame, minimizzando sempre l'errore quadratico medio\footnote{Chiamato in inglese MSE ovvero \textit{Mean Squared Error} che identifica statisticamente la precisione di una stima rispetto al valore che si sta cercando di stimare. Nel filtro di Kalman è utilizzato per quantificare quanto sia accurata la stima dello stato del sistema dinamico osservato.} della stima da realizzare come valore atteso:
\begin{equation}
MSE=E[(x_k-\hat x_k)^2]
\end{equation}
Nella quale il termine \(\hat x_k\) identifica la stima di \(x_k\). 

È inoltre importante notare, che per minimizzare l'errore quadratico medio, vi deve essere la possibilità di poter bilanciare l'informazione proveniente dalla previsione, con quella proveniente dalle nuove osservazioni e misurazioni realizzate, tale parametro è denominato guadagno di Kalman.\newline
Il guadagno di Kalman \(K_k\) (a volte riportato anche come \(L_k\)) è utile per ottimizzare la combinazione tra la previsione realizzata dal modello in uso e la misurazione realizzata, consentendo quindi di minimizzare l'errore di stima ad ogni istante in cui esso viene modificato. Tale guadagno è quindi calcolato in maniera tale da pesare le informazioni in base alla loro affidabilità. Relativamente a ciò si può comprendere tale meccanismo in questa maniera: se si è certi che la misura realizzata sia precisa, allora il guadagno affiderà alla misura maggior rilevanza; ciò permette al filtro di poter essere flessibile rendendolo adattabile al sistema durante il suo utilizzo, nel caso contrario tale misura verrà scartata attendendone una più attendibile.\newline
%retroazione nel filtro di Kalman
È importante soffermarsi in maniera precisa per comprendere maggiormente il funzionamento del filtro di Kalman, affermando infatti che la stima del sistema venga aggiornata ai passi successivi a quello osservato (grazie alla misurazione realizzata sul sistema stesso nella fase di aggiornamento) vuole significare che all'interno del diagramma a blocchi che descrive il flusso di funzionamento dell'algoritmo, potremo notare una retroazione realizzata dalla misurazione del valore del sistema, che torna indietro verso il passo di predizione.\newline

Andando a osservare il testo di Kalman~\cite{Kalman1960} troviamo riportate queste parole:
\begin{displayquote}
By inspection of Fig. 3 we see that the optimal filter is a feedback system, and that the signal after the first summer is white noise since \(\tilde y (t|t - 1)\) is obviously an orthogonal random process. This corresponds to some well-known results in Wiener filtering [\ldots] However, this is apparently the first rigorous proof that every Wiener filter is realizable by means of a feedback system.~\cite{Kalman1960}\footnote{\textit{Eseguendo un'analisi della Fig. 3, si osserva che il filtro ottimo è un sistema con retroazione, e che il segnale dopo il primo sommatore è rumore bianco poiché \(\tilde y(t|t - 1)\) è evidentemente un processo randomico ortogonale. Ciò corrisponde a risultati ben noti nella teoria del filtraggio di Wiener. Tuttavia, quello qui riportato sembra essere il primo risultato rigoroso che dimostra che ogni filtro di Wiener è realizzabile mediante un sistema con retroazione.} (Traduzione in italiano a cura dell'autore di questo testo.)}
\end{displayquote}


%%%=======================================

\begin{figure}[hbt!]
\begin{center}
\includegraphics[width=12cm]{figure/fig3_kalman1960.png}
\caption{Rielaborazione della figura riportante il diagramma a blocchi del filtro di Kalman (la numero 3 menzionata dallo stesso autore) realizzata da John Lukesh nel 2002 per la riedizione dell'articolo introduttivo del filtro di Kalman~\cite{Kalman1960}.}
\end{center}
\end{figure}\label{fig:fig-diag-Kalman}

%a cosa è utile
Da come si può riscontrare, tale tipologia, sotto ipotesi di linearità e rumore gaussiano, è una soluzione ottima per la stima dello stato di un sistema dinamico~\cite{Kalman1960}; inoltre tale algoritmo acquisisce robustezza grazie al guadagno di Kalman che permette di fornire misure stabili e precise anche in presenza di rumore o mancanza di informazioni; se ben realizzato è efficiente a livello computazionale per sistemi di dimensioni moderate. Possiamo infatti affermare che la complessità computazionale di tale filtro se implementato come algoritmo è molto dipendente dal numero di stati che il sistema può assumere che possono far crescere di molto la grandezza delle matrici utilizzate. In linea generale e senza entrare nel dettaglio, la complessità computazionale dello stesso è \(O(n^2)\), che può sembrare notevole, ma esso per un numero di stati del sistema elevato, risulta però più preciso, efficiente e meno influenzato da rumori rispetto ad altri algoritmi per la stima dello stato di un sistema.
%fine di Il filtro di Kalman: stimatore ottimo
\newline
In breve tale tipologia di stimatore, permette di realizzare algoritmi che implementino assistenti di guida (come quelli per la guida autonoma), navigatori, strumenti per dare maggior precisione alla computer vision, applicazioni più svariate al signal processing, robotica, etc \ldots Come precedentemente descritto, uno dei primi utilizzi di tale filtro, fu l'implementazione del filtro di Kalman esteso\footnote{Descritto con maggiore precisione nel seguito,} per il sistema che stimava la traiettoria del modulo lunare della missione della NASA Apollo 11~\cite{nasaReconstructionApollo}.
Proprio quest'ultima tipologia di filtro, venne implementata, poiché il filtro di Kalman, per come descritto dal suo creatore, poteva essere applicato ed utilizzato solamente per sistemi lineari che presentino rumori gaussiani all'interno del processo. Nel mondo reale siamo però circondati da sistemi non lineari con rumori non gaussiani. Si rese quindi necessario lo sviluppo di metodi più avanzati (come il filtro di Kalman esteso), per ottenere stime accurate anche in caso di sistemi non lineari.
L’articolo di Kalman~\cite{Kalman1960} segna quindi una svolta storica nella teoria del controllo e dell’elaborazione dei segnali, offrendo una soluzione elegante e potente a problemi complessi di stima. L'influenza del filtro di Kalman è ancora oggi evidente, con applicazioni che spaziano dall’ingegneria alla finanza, etc... Il suo metodo rimane uno degli strumenti più studiati e utilizzati per la stima di stati dinamici in ambienti incerti.

\section{Tipologie di filtro di Kalman}

Come riportato precedentemente, da ora in poi utilizzeremo il cognome del matematico ed ingegnere Rudolf Emil Kalman, senza accenti, riferendoci al filtro sempre in tale maniera. Come già accennato, tale tipologia di filtro, divenuta molto popolare nello scorso secolo, deve la sua fama alle numerose mutazioni, estensioni e generalizzazioni che sono comparse dello stesso.
Sebbene il funzionamento basilare del filtro rimanga comune a tutte le applicazioni specifiche che osserveremo, nelle tipologie che andremo ad osservare le possibilità di applicazione che esse hanno generato, hanno allargato lo spettro di utilizzo dello stimatore rispetto alle prospettive di utilizzo iniziali.\newline
Vengono quindi riportate di seguito alcune delle tipologie più note del filtro di Kalman. Il criterio utilizzato per la scelta di quali tipologie menzionare non è casuale, ma si è pensato di riportare quelle mutazioni del filtro, che hanno cambiato la percezione di cosa un filtro di Kalman possa effettivamente fare.
Di seguito verrano riportati i nomi dei filtri in lingua inglese, poiché un'italianizzazione degli stessi sarebbe una forzatura inutile e non permetterebbe di capire di cosa si sta trattando. A puro titolo d'esempio osserviamo che quello che fino ad ora abbiamo chiamato filtro di Kalman esteso, prenderà il nome di \textit{Extended Kalman Filter}, seguito tra parentesi dall'acronimo utilizzato in genere durante il suo comune utilizzo, ovvero \textit{(EKF)}.

\subsection{Filtro di Kalman o Canonical Kalman Filter}
Essa è la versione che Kalman arrivò a formulare nel 1960, realizzata per l'applicazione ai sistemi lineari e proposta nel suo scritto \textit{A new approach to linear filtering and prediction problems}~\cite{Kalman1960}, riportiamo di seguito un breve riepilogo delle informazioni raccolte sino ad ora sullo stesso, ai fini della completezza di questo testo.\newline
%inserire riferimenti al fatto che vi sia una retroazione
Il filtro di Kalman "canonico" permette di osservare e quindi stimare ciò che non si può verificare, vedere o misurare direttamente. Esso è infatti una particolare tipologia di sistema dinamico appartenente alla categoria degli osservatori di stato (nella teoria del controllo). Implementa un modello matematico accurato che permette di calcolare un'approssimazione, il più delle volte corretta, del valore che si sta stimando, infatti tale stima è perfezionata nel tempo dallo stesso filtro innestando all'interno del processo di "filtraggio" misurazioni ottenute dal sistema stesso nei vari istanti. In pratica il funzionamento si basa sulla minimizzazione della differenza che vi è tra un valore stimato ed un valore misurato dal sistema, per mezzo di una retroazione (chiamata in inglese \textit{feedback}) ed un controllore con un proprio guadagno che chiameremo \(K\). La scelta del valore di tale guadagno, chiamato \textit{guadagno di Kalman} permette di bilanciare ad ogni istante di tempo la rilevanza che possa dare la misurazione dal sistema rispetto alla stima realizzata e viceversa. Stiamo quindi affermando che di base controlleremo la pendenza della curva di decadimento dell'errore che può esservi nel processo di stima, tramite il feedback, infatti, più velocemente riusciremo a portare a zero tale errore e più velocemente otterremo una stima del valore vicina al valore che si sarebbe voluto stimare (in pratica minimizzando l'errore aumentiamo la precisione della stima del valore). Spesso si fa riferimento al valore voluto con \(x\) ed al valore stimato con \(\hat x \).\newline
Nella figura precedentemente riportata \ref{fig:fig-diag-Kalman}, tratta dall'articolo di Kalman i parametri utilizzati sono leggermente diversi da come li avevamo descritti nella sezione introduttiva al filtro di Kalman, riportiamo pertanto i parametri aggiornati:
\begin{itemize}
\item{L'equazione di stato che ricordiamo descrivere l'evoluzione del sistema da uno stato temporale all'altro, tenendo conto della dinamica del sistema e della rumorosità del processo è così ri-scrivibile: 
\begin{equation}
x(t+1)=\phi(t+1;t)x(t)+w(t)
\end{equation} 
Nella quale \(x(t)\) è lo stato del sistema all'istante t, \(\phi(t+1;t)\) è la matrice di transizione di stato dello stato che si evolve da \(t\) a \(t+1\) e \(w(t)\) è il rumore di processo con media nulla e covarianza nota.
}
\item{L'equazione di misura che collega lo stato del sistema alle osservazioni misurate includendo quindi anche il rumore di misura sarà invece molto più simile a quella riportata precedentemente: \begin{equation}y(t)=H(t)x(t)+v(t)\end{equation} Nella quale \(y(t)\) è il vettore delle misure osservate al tempo \(t\), \(H(t)\) è la matrice di osservazione  che è in grado di mappare lo stato del sistema \(x(t)\) allo spazio delle osservazioni dello stesso e \(v(t)\) è il rumore di misura sempre con media nulla e covarianza nota.
}
\end{itemize}

\subsection{Extended Kalman Filter (EKF)}\label{se:ekf-spiegazione}

Essa è l'applicazione del filtro di Kalman che ne prevede l'utilizzo per sistemi non lineari, infatti, ogni passaggio dell'algoritmo prevede una approssimazione basata su di una linearizzazione realizzata localmente. Tale linearizzazione è calcolata tramite la matrice Jacobiana\footnote{Essa è una particolare tipologia di matrice che fa uso delle derivate parziali per rappresentare la linearizzazione locale della funzione che gli viene introdotta all'interno. In pratica avendo una funzione vettoriale \(f: \mathbb{R}^{n} \rightarrow  \mathbb{R}^{m}\)  a più variabili e volendola linearizzare, la Jacobiana permette di mettere insieme le derivate parziali di ogni singola variabile all'interno di una matrice \(m \times n\).} delle funzioni di transizione di stato e di quelle di osservazione per ogni istante temporale. Questo approccio riportato, introduce un errore di approssimazione che può essere tollerabile se la non linearità è sufficientemente bassa o se l'intervallo di tempo tra le iterazioni di aggiornamento è piccolo, riducendo l'accumulo di errori complessivo.\newline
Questa tipologia di estensione del filtro di Kalman, venne proposta per la prima volta dalla NASA, durante la realizzazione del sistema per tracciare la traiettoria del modulo lunare per le missioni \textit{Apollo}~\cite{nasaDiscoveryKalman}, come riportato nell'immagine:
\begin{figure}[hbt!]
\centering
\includegraphics[width=12cm]{figure/NASA_extended_Kalman_filter.png}\caption{Note dal documento della NASA~\cite{nasaDiscoveryKalman} riportanti la versione dello stimatore \textit{estesa} per poter essere utile per il calcolo della traiettoria della modulo lunare dell'Apollo 11 verso il suolo lunare.}
\end{figure}

A livello computazionale, tale estensione è molto più onerosa rispetto al filtro di Kalman standard, tale algoritmo deve infatti soffermarsi sui calcoli per la linearizzazione realizzata tramite la Jacobiana, che nel caso di un numero elevato di stati e/o osservazioni richiede un grande carico computazionale strettamente collegato quindi alle specifiche del sistema.\newline



\begin{figure}[hbt!]
\begin{center}
   % \vspace{0.5cm} % Spazio sopra il diagramma
\begin{tikzpicture}[node distance=2cm]

% Nodes
\node (start) [process] {INPUT};
\node (init) [process, below of=start] {Inizializzazione del vettore di stato stimato};
\node (predict) [process, below of=init] {Predizione del vettore di stato};
\node (jacobian) [process, below of=predict] {Calcolo della matrice di osservazione, ovvero della Jacobiana della misura non lineare};
\node (measure) [process, below of=jacobian] {Acquisizione di un nuovo vettore di misura};
\node (update) [process, below of=measure] {Calcolo del vettore di stato aggiornato};
\node (output) [process, below of=update] {OUTPUT};
\node (increment) [process, left of=predict, xshift=-6cm] {$k = k + 1$};

% Arrows
\draw [arrow] (start) -- (init);
\draw [arrow] (init) -- (predict);
\draw [arrow] (predict) -- (jacobian);
\draw [arrow] (jacobian) -- (measure);
\draw [arrow] (measure) -- (update);
\draw [arrow] (update) -- (output);
\draw [arrow] (output) -| (increment);
\draw [arrow] (increment) -- (predict);

\end{tikzpicture}
%\includegraphics[width=8cm]{figure/EKF_schema.png}
\caption{Schema a blocchi del funzionamento di un filtro di Kalman esteso, in cui possiamo notare la linearizzazione del sistema attuata tramite la Jacobiana.\iffalse fonte https://www.researchgate.net/publication/347358586_Sensorless_Voltage_Observer_for_a_Current-Fed_High_Step-Up_DC-DC_Converter_Using_Extended_Kalman_Filter \fi}
\end{center}
\end{figure}

Seppure può sembrare elevato il costo computazionale di questa estensione, è comunque conveniente l'utilizzo di tale stimatore rispetto ad altri, per via della sua precisione e praticità di utilizzo, è per tale motivo che essa è l'estensione scelta per il seguito della trattazione che vedremo nei capitoli successivi e che trova numerose applicazioni nel campo dell'ingegneria. Oltre al già menzionato e famoso utilizzo in termini aerospaziali, questa variante del filtro di Kalman, è utilizzata nei sistemi di localizzazione e posizionamento GPS di veicoli, oltre all'applicazione al controllo di robot e a numerosissimi altri campi della ricerca e della tecnologia, come l'audio.\newline
In breve, essa rappresenta una soluzione efficace per la stima degli stati in sistemi non lineari con elevati costi computazionali che necessità una conoscenza adeguata del sistema in esame che possa garantire che l'approssimazione locale che si va a realizzare non introduca moltissimi errori, è inoltre importante constatare che il sistema che si vuole linearizzare non sia discontinuo, infatti una tale tipologia di sistema non permette il calcolo delle derivate, andando ad invalidare le operazioni di linearizzazione locale realizzate dalla Jacobiana.  

\newpage
\subsection{Unscented Kalman Filter (UKF)}\label{se:ukf-spiegazione}

Esso, come il filtro di Kalman esteso, è una variante dello stesso progettata per l'utilizzo con sistemi non lineari. Supera alcune limitazioni di quest'ultimo (che ricordiamo linearizza un sistema non lineare tramite approssimazioni del primo ordine realizzate tramite derivate parziali, ovvero tramite l'uso di Jacobiane) ed utilizza invece la \textit{Unscented Transform} che permette una rappresentazione più accurata della distribuzione della probabilità degli stati del sistema per mezzo di un insieme di punti definito \textit{punti sigma} che realizzano una rappresentazione statistica avanzata delle distribuzioni, tramite una distribuzione statistica dello stesso~\cite{Julier1997}.

\subsection{Ensemble Kalman Filter (EnKF)}

Con tale termine viene indicata una variante del filtro di Kalman utilizzata spesso per sistemi dinamici nei quali il modello matematico che si vuole utilizzare ha un gran numero di variabili ed una struttura altamente non lineare, come per la meteorologia e l'oceanografia. Esso si basa quindi su un approccio probabilistico, che propone un "ensemble" (ovvero un gruppo) di possibili stati del sistema (e non una singola stima dello stesso) che rappresentano ciascuno una possibile realizzazione del sistema. Ognuno di essi viene aggiornato singolarmente, il filtro da ultimo stima la media e la covarianza dell'insieme per ottenere un'approssimazione dell'evoluzione probabilistica dello stato. In breve esso permette di sfruttare un insieme di possibili stati che evolve nel tempo e viene corretto con i dati osservativi.~\cite{Evensen2003}

\subsection{Square Root Kalman Filter}

Essa è una variante del filtro di Kalman canonico, che utilizza matrici delle covarianze\footnote{Le matrici delle covarianze, sono particolari matrici che descrivono la variabilità e la correlazione che vi è tra diverse variabili casuali di un sistema. In applicazioni relative al filtro di Kalman, tali matrici rappresentano l'incertezza che vi è nello stima dello stato del sistema e quella che vi può essere nelle misure che vengono realizzate sul sistema stesso.} sotto radici quadrate, al posto di utilizzare matrici intere per ovviare ad eventuali problemi numerici che potrebbero generarsi. Quindi tale tipologia di filtro, garantisce una stabilità numerica maggiore del filtro di Kalman tradizionale oltre ed una precisione incrementale nell'utilizzo dello stesso.

\subsection{Deep Kalman Filter}

Essa è un'estensione del filtro di Kalman che combina le reti neurali\footnote{Per \textit{Deep Neural Network} o rete neurale "profonda" si intende una particolare rete neurale artificiale, realizzata attraverso la composizione di nodi che processano i dati in maniera gerarchica. Sono definite "profonde", poiché si avvalgono di più strati di nodi (o neuroni artificiali) che consento di rappresentare ed apprendere strutture complesse di dati.} e il filtro di Kalman stesso per la gestione e l'elaborazione di segnali non lineari. Esso risulta utile laddove sono di difficile definizione i modelli del sistema in esame e le relazioni tra le variabili come ad esempio nella computer vision, nella robotica o nei sistemi biologici.