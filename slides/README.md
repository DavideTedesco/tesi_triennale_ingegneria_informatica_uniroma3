# Applicazione del filtro di Kalman all'elaborazione di segnali audio

## Slide 1 (_slide iniziale_)

- Buon pomeriggio...
- introduzione, contesto tecnologico e di applicazione

## Slide 2 (Il filtro di Kalman: storia, potenzialità, estensioni e generalizzazioni)

- l'utilizzo dei filtri risale all'antichità
- essi erano dispositivi realizzati in materiali porosi utilizzati per ottenere, ad esempio, una parte di liquido piú sottile di quella posta in ingresso _come quello che osserviamo in figura_
- un filtro, a livello ingegneristico e in maniera più astratta è una sorta di scatola nera nella quale viene inserito all'ingresso un qualcosa ottenendo in uscita un'elaborazione dello stesso
- il filtro che trattiamo è il filtro di Kalman: particolare tipologia di filtro definita __stimatore__
- esso è utile per stimare lo stato di sistemi lineari
- viene utilizzato spesso in due casi:
  - quando si vuole realizzare la stima di un sistema quando il valore non può essere stimato direttamente
  - quando è necessario stimare lo stato di un sistema soggetto a rumori

## Slide 3 (Il filtro di Kalman: storia, potenzialità, estensioni e generalizzazioni)

- tale tipologia di stimatore è realizzata attraverso due fasi, al fine di ottenere una stima ottima del valore voluto
  1. __fase di predizione__, realizzata tramite delle equazioni che modellano il funzionamento del sistema
  2. __fase di aggiornamento__, in cui data una misura tratta dal sistema, la si combina con la predizione, migliorando la stima tramite una retroazione (ovvero un feedback) _come è possibile osservare nell'immagine della quale non entriamo nel dettaglio, ma che presenta una freccia (la più esterna) che va a sottrarre alla prima sommatoria l'uscita del processo filtrato $\overline y(t|t-1)$_
- nello specifico useremo il filtro di Kalman nella sua estensione denominata EKF
- essa venne introdotta dalla NASA per poter calcolare la traiettoria del modulo lunare delle missioni Apollo
- essa è in grado di stimare il valore di sistemi non lineare
- introducendo prima del passaggio della predizione, una linearizzazione dei valori in ingresso

## Slide 4 (Filtraggio audio analogico e digitale)

- il filtro in ambito audio è quindi un dispositivo analogico o digitale che permette di elaborare il segnale che viene posto in ingresso ad esso
- _come osserviamo nell'immagine un rumore bianco che passa attraverso un filtro passa basso, cambia il suo contenuto frequenziale_
- il legame tra i filtri in ambito audio ed il filtro di Kalman è molto sottile ed è difficile da spiegare in poche parole
- si può però osservare che i filtri come vengono utilizzati in genere in ambito audio, sono osservati ed utilizzati nel dominio della frequenza, mentre il filtro di Kalman è osservato ed utilizzato nel dominio del tempo
- si può quindi affermare in maniera parziale che solo sotto alcune condizioni il filtro di Kalman è semplicemente osservabile nel dominio delle frequenze

## Slide 5 (Utilizzi, applicazioni pratiche ed alcuni strumenti software che implementano il filtro di Kalman)

- è possibile utilizzare il filtro di Kalman con sistemi non lineari
- interrogandomi su quali strumenti poter utilizzare mi sono imbattuto nei più comuni strumenti commerciali come MATLAB e Simulink
- ho voluto poi cercare versioni Open Source degli stessi come Octava, Scilab & Xcos
- ed infine ho trovato lo strumento che ho utilizzato nel seguito per il caso di studio ovvero Python
- è inoltre stato importante comprendere in quali termini sia possibile l'utilizzo del filtro di Kalman in tempo reale o in tempo differito

## Slide 6 (Possibili applicazioni del filtro di Kalman per l’audio)

- tra le possibili applicazioni del filtro di Kalman osservate in letteratura (che trovate riportate)
-  ho deciso di di studiare più da vicino l'ultima di esse: la restaurazione dei segnali audio

## Slide 7 (Esempio di applicazione pratica del filtro di Kalman all’audio: restaurazione di un file audio)

- è stato quindi utilizzato il filtro di Kalman esteso (che implica linearizzazione, predizione e aggiornamento)
- basando il modello del processo del sistema in esame su modelli autoregressivi (AR) come descritto in letteratura
- i modelli AR sono utili a descrivere il processo stocastico di un segnale sonoro che varia nel tempo
- _osservando l'immagine che descrive il funzionamento dell'algoritmo vediamo che vi è oltre alla retroazione implicita nell'EKF (visto come una scatola chiusa) anche una retroazione che proviene dall'Outlier detector_
- nell'algoritmo presentato vengono infatti identificate due tipologie diverse di rumore:
  - rumori di fondo
  - rumori identificati tramite click, essi sono chiamati _outliers_ e sono identificati tramite una variabile (una flag), se essi vengono rilevati l'algoritmo cerca di rimuoverli

## Slide 8 (Esempio di applicazione pratica del filtro di Kalman all’audio: restaurazione di un file audio)
- senza entrare nel dettaglio di ogni funzione specifica
- è utile osservare che tra le funzioni definite ve ne sono alcune fondamentali sino ad ora viste:
  - `linearize`, utile per rendere lineare il sistema non lineare posto in ingresso
  - `predict` e `update` che fanno parte del cuore del filtro di Kalman ed insieme al `linearize` formano il filtro di Kalman esteso
  - `detect_click` per il riconoscimento degli outliers

## Slide 9 (Esempio di applicazione pratica del filtro di Kalman all’audio: restaurazione di un file audio)

- è stato importante selezionare alcune tipologie di segnali diverse per comprendere il funzionamento dell'algoritmo
- il primo file audio utilizzato non aveva alcun rumore, e si è aggiunto rumore ad esso per poter comprendere se vi fosse l'introduzione aberrazioni tramite il filtraggio
- è stato aggiunto sia rumore di fondo, che rumore che simuli la presenza di click

## Slide 10 (Esempio di applicazione pratica del filtro di Kalman all’audio: restaurazione di un file audio)

- si sono poi osservati due campioni diversi
- uno con molta presenza di click, relativo alla prima registrazione di Edison tramite un fonografo

## Slide 11 (Esempio di applicazione pratica del filtro di Kalman all’audio: restaurazione di un file audio)
- un'altro molta presenza di rumore di fondo, relativo alla registrazione di Armstrong che compie il primo passo sul suolo lunare

## Slide 12 (Conclusioni, sviluppi futuri e considerazioni sui risultati numerici e sonori ottenuti)

- Giungendo verso la conclusione...
- si è compreso che l'algoritmo realizzato ha una forte dipendenza dal calcolo matriciale che risulta pesante per file audio di una certa lunghezza, il che implica una grande necessità di memoria RAM
- è necessario ulteriore testing con altre tipologie di segnali
- sarebbe necessario utilizzare una metodologia che applichi la programmazione concorrente cercando di portare l'algoritmo verso il real-time dato che esso è oggi solo funzionante in tempo differito
- Si pensa inoltre che lo studio e l'applicazione debba proseguire per portare questo ed altri strumenti e teorie dai loro campi primari di applicazione a nuovi campi di ricerca tramite la creatività.

## Slide 13 (_qr code & citazione_)

- Tramite il qr code è possibile visionare la tesi, il codice e queste slides
- Grazie
