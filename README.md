# Tesi_Triennale_Ingegneria_Informatica_uniroma3

Contenuto del repository:
1. `appunti_schermate` -> contiene screenshot di appunti utilizzati durante lo studio e la realizzazione della tesi
2. `materiali` -> contiene la maggior parte dei materiali di letteratura utilizzati durante la tesi
3. `slides` -> contiene le slides realizzate per la presentazione della tesi ed il suo codice sorgente
4. `src` -> contiene il codice e gli esempi utilizzati o esportati dal codice realizzato
5. `tesi` -> contiene la tesi ed il suo codice sorgente
